<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', [
    'namespace' => 'App\Http\Controllers\Api\V1',//命名空间
    'middleware' => ['serializer:array','bindings']//切换返回数据格式
], function($api) {
    //登录
    $api->post('shop/{shop}/authorizations', 'AuthorizationsController@store')->name('api.authorizations.store');
    //刷新Token
    $api->put('authorizations/current', 'AuthorizationsController@update')->name('api.authorizations.update');

    //测试
    // $api->post('sites', 'SiteController@store')->name('api.site.store');
    // $api->delete('sites/{id}', 'SiteController@destroy')->name('api.site.destroy');


    //需要验证的接口
    $api->group(['middleware' => 'api.auth'], function($api){

        //Qrcode
        $api->get('qrcode', 'QrcodeController@show')->name('api.qrcode.show');
        
        //FormId
        $api->post('formids', 'FormIdController@store')->name('api.formids.store');

        //我的
        //获取当前用户信息
        $api->get('user', 'UserController@me')->name('api.user.show');
        //评论过的动态
        $api->get('user/commented_updates', 'UpdateController@userCommentedIndex')->name('api.user.commented_updates.index');
        //点赞过的动态
        $api->get('user/liked_updates', 'UpdateController@userlikedIndex')->name('api.user.liked_updates.index');
        //我的团队
        $api->get('user/sites/members', 'SiteMemberController@userIndex')->name('api.user.sites.members.index');

        //------------------

        //门店信息
        $api->get('shop','ShopController@current')->name('api.shop.current');
        //门店文章
        $api->get('shop/article', 'ArticleController@shopShow')->name('api.shop.article.show');
        //门店资质
        $api->get('shop/qualifications', 'QualificationController@shopIndex')->name('api.shop.qualifications.index');
        //门店-小区列表（包含每个小区工地数）
        $api->get('shop_estates', 'EstateController@shopIndex')->name('api.shop.estates.index');

        //门店-地区(包含小区)
        $api->get('shop_regions', 'RegionController@shopIndex')->name('api.shop.regions.index');
        //公司装修风格
        $api->get('shop_styles', 'StyleController@index')->name('api.shop.styles.index');
        //公司时间线
        $api->get('shop_timelines', 'TimelineController@index')->name('api.shop.timelines.index');

        //------------------

        //团队
        //团队列表
        $api->get('teams', 'TeamController@index')->name('api.teams.index');
        //团队中的员工
        $api->get('teams/{teamId}/staff', 'StaffController@index')->name('api.teams.staff.index');
        //员工详情
        $api->get('staff/{id}',  'StaffController@show')->name('api.staff.show');
        //员工动态
        $api->get('staff/{staff}/updates',  'UpdateController@staffIndex')->name('api.staff.updates.index');
        //员工添加客户
        $api->post('staff/{code}/user', 'StaffController@staffStore')->name('api.staff.users.store');

        //------------------

        //工地
        //工地列表
        $api->get('sites', 'SiteController@index')->name('api.sites.index');
        //工地详情
        $api->get('sites/{id}', 'SiteController@show')->name('api.sites.show');
        //工地动态
        $api->get('sites/{id}/updates', 'UpdateController@index')->name('api.sites.updates.index');
        $api->get('updates/{id}', 'UpdateController@show');
        $api->get('sites/{id}/evaluations', 'EvaluationController@siteIndex')->name('api.sites.evaluations.index');
        //点赞动态
        $api->post('likes', 'LikeController@store')->name('api.likes.store');
        //取消点赞动态
        $api->delete('likes', 'LikeController@destroy')->name('api.likes.destroy');
        //发布评论
        $api->post('comments', 'CommentController@store')->name('api.comments.store');
        //删除评论
        $api->delete('comments/{id}', 'CommentController@destroy')->name('api.comments.destroy');
        //工地的团队成员
        $api->get('sites/{id}/members', 'SiteMemberController@index')->name('api.sites.members.index');
        //工地关注情况
        $api->get('sites/{id}/follow', 'FollowerController@show')->name('api.sites.follower.show');
        //工地关注
        $api->put('sites/{id}/follow', 'FollowerController@follow')->name('api.sites.follower.follow');
        //工地取关
        $api->put('sites/{id}/unfollow', 'FollowerController@unfollow')->name('api.sites.follower.unfollow');
        //工地围观，关注列表
        $api->get('sites/{id}/followers', 'FollowerController@siteIndex')->name('api.sites.followers.index');
        //评价
        $api->get('evaluations', 'EvaluationController@index')->name('api.evaluations.index');
        $api->get('recommend_evaluations', 'EvaluationController@recommendIndex')->name('api.evaluations.recommend.index');
        $api->post('evaluations', 'EvaluationController@store')->name('api.evaluations.store');

        //------------------
        
        //产品套餐
        $api->get('products', 'ProductController@index')->name('api.products.index');
        $api->get('products/{id}', 'ProductController@show')->name('api.products.show');

        //------------------

        //广告
        $api->get('ads', 'AdController@index')->name('api.ads.index');
        $api->get('ads/{id}', 'AdController@show')->name('api.ads.show');

        //------------------

        //案例
        $api->get('recommend_cases', 'CaseController@recommendIndex')->name('api.cases.recommend.index');
        $api->get('cases', 'CaseController@index')->name('api.cases.index');
        $api->get('cases/{id}', 'CaseController@show')->name('api.cases.show');

        //------------------

        //咨询记录
        $api->post('consulting_records', 'ConsultingRecordController@store')->name('api.consultingRecords.store');
    
        $api->get('comments', 'CommentController@index');

        //-----------------

        //用户行为
        $api->post('user/behavior', 'UserBehaviorController@store')->name('api.user.behavior.store');

        //动态
        //$api->get('updates', 'UpdateController@index')->name('api.udpates.index');
        //$api->get('updates/{id}', 'UpdateController@show')->name('api.udpates.show');
    });
    // $api->get('users/{user}/comments', 'CommentController@userIndex')->name('api.users.comments.index');
    // $api->get('comments', 'CommentController@index')->name('api.comments.index');
    // $api->get('comments/{id}', 'CommentController@show')->name('api.comments.show');
    // $api->group([
    //     'middleware' => 'api.throttle',
    //     'limit' => 2,
    //     'expires' => 1
    // ], function($api){
    //     $api->post('users', 'UserController@store')->name('api.users.store');
    // });
});