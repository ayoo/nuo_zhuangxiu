<?php

return [

    //Model
    'models' => [
        '01' => 'shop',                     //门店
        '02' => 'consulting_record',        //咨询
        '03' => 'product',                  //产品
        '04' => 'ad',                       //广告
        '05' => 'case',                     //案例
        '06' => 'team',                     //团队
        '07' => 'staff',                    //员工
        '08' => 'evaluation',               //评价
        '09' => 'site',                     //工地
        '10' => 'update'                    //动态
    ],

    //Behavior
    'behaviors' => [
        '01' => 'list',                     //查看列表
        '02' => 'view',                     //产看详细
        '03' => 'submit',                   //提交
        '04' => 'share',                    //分享
        '05' => 'view_about',               //查看关于我们
        '06' => 'list_case',                //案例列表
        '07' => 'list_evaluation',          //评价列表
        '08' => 'follow',                   //关注
        '09' => 'follow_delete',            //取消关注
        '10' => 'comment',                  //评论
        '11' => 'comment_delete',           //删除评论
        '12' => 'like',                     //点赞
        '13' => 'like_detete',              //取消点赞
        '14' => 'filter',                   //筛选
        '15' => 'filter_update',            //筛选动态
        '16' => 'add_to_book',              //添加到通讯录
        '17' => 'become_customer',          //成为客户
        '18' => 'filter_estate',            //筛选小区
        '19' => 'filter_style',             //筛选风格
        '20' => 'filter_stage',             //筛选阶段
    ],

    //Model_Behavior
    'model_behavior' => [
        '0105' => [ 'm' => 'shop', 'b' => 'view_about', 'msg' => '看了关于我们'],
        '0203' => [ 'm' => 'consulting_record', 'b' => 'submit', 'msg' => '提交了咨询'],
        '0302' => [ 'm' => 'product', 'b' => 'view', 'msg' => '看了产品套餐 %s'],
        '0402' => [ 'm' => 'ad', 'b' => 'view', 'msg' => '看了广告 %s'],
        '0502' => [ 'm' => 'case', 'b' => 'view', 'msg' => '看了精选案例 %s'],
        '0602' => [ 'm' => 'team', 'b' => 'view', 'msg' => '看了 %s'],
        '0702' => [ 'm' => 'staff', 'b' => 'view', 'msg' => '看了员工 %s 的名片'],
        '0706' => [ 'm' => 'staff', 'b' => 'list_case', 'msg' => '看了 %s 的精选案例'],
        '0716' => [ 'm' => 'staff', 'b' => 'add_to_book', 'msg' => '添加了 %s 的联系方式'],
        '0717' => [ 'm' => 'staff', 'b' => 'become_customer', 'msg' => '成为 %s 的客户'],
        '0801' => [ 'm' => 'evaluation', 'b' => 'list', 'msg' => '添加了 %s 的联系方式'],
        '0901' => [ 'm' => 'site', 'b' => 'list', 'msg' => '看了工地列表'],
        '0902' => [ 'm' => 'site', 'b' => 'view', 'msg' => '看了工地 %s'],
        '0904' => [ 'm' => 'site', 'b' => 'share', 'msg' => '分享了工地 %s'],
        '0907' => [ 'm' => 'site', 'b' => 'list_evaluation', 'msg' => '产了工地 %s 的评价'],
        '0908' => [ 'm' => 'site', 'b' => 'follow', 'msg' => '关注了工地 %s'],
        '0909' => [ 'm' => 'site', 'b' => 'follow_delete', 'msg' => '取关了工地 %s'],
        '0910' => [ 'm' => 'site', 'b' => 'comment', 'msg' => '评论了工地 %s'],
        '0911' => [ 'm' => 'site', 'b' => 'comment_delete', 'msg' => '删除了评论'],
        '0912' => [ 'm' => 'site', 'b' => 'like', 'msg' => '点赞了工地 %s'],
        '0913' => [ 'm' => 'site', 'b' => 'like_delete', 'msg' => '取消了点赞'],
        '0914' => [ 'm' => 'site', 'b' => 'filter', 'msg' => '筛选工地'],
        '0918' => [ 'm' => 'site', 'b' => 'filter_estate', 'msg' => '筛选工地'],
        '0919' => [ 'm' => 'site', 'b' => 'filter_style', 'msg' => '筛选工地'],
        '0920' => [ 'm' => 'site', 'b' => 'filter_stage', 'msg' => '筛选工地'],
        '0915' => [ 'm' => 'site', 'b' => 'filter_update', 'msg' => '筛选了工地 %s %s 阶段的动态'],
        '1010' => [ 'm' => 'update', 'b' => 'comment', 'msg' => ''],
        '1011' => [ 'm' => 'update', 'b' => 'comment_delete', 'msg' => ''],
        '1012' => [ 'm' => 'update', 'b' => 'like', 'msg' => ''],
        '1013' => [ 'm' => 'update', 'b' => 'like_delete', 'msg' => '']
    ]

];