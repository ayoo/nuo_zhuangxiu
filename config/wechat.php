<?php

return [
    'miniprogram' => [
        'code2session_url' => 'https://api.weixin.qq.com/sns/jscode2session'
    ],
    'qrcode' => [
        //工地海报
        'site' => [
            'scene' => 'id=%s',
            'page'  => 'pages/site/site',
            'width' => 231 
        ],
        //名片海报
        'staff' => [
            'scene' => 'share=1&id=%s',
            'page'  => 'pages/staff/staff',
            'width' => 231 
        ],
        //门店首页小程序码
        'home' => [
            'scene' => 'scene',
            'page'  => 'pages/index/index',
            'width' => 462
        ],
        'repo' => [
            'home' => 'App\Models\Shop',
            'site' => 'App\Models\Site',
            'staff' => 'App\Models\Staff'
        ]
    ]
];