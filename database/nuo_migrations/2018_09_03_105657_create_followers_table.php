<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('site_id')->comment('工地ID');
            $table->unsignedInteger('user_id')->comment('用户ID');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedTinyInteger('status')->comment('状态：0围观，1关注');

            $table->timestamps();
            $table->softDeletes();
            $table->unique(['site_id','user_id']);
            $table->comment = '围观关注表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('followers');
    }
}
