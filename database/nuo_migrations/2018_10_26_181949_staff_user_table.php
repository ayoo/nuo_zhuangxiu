<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_user', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedInteger('staff_id')->comment('员工ID');
            $table->unsignedInteger('user_id')->comment('用户ID');
            $table->unsignedTinyInteger('status')->comment('类型：0：普通，1：意向客户，2：签约用户')->default(0);
            $table->string('scene', 10)->comment('场景');

            $table->timestamps();
            $table->softDeletes();
            $table->comment = '员工客户关系表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_user');
    }
}
