<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedInteger('staff_id')->comment('员工ID');
            $table->unsignedInteger('timeline_id')->comment('时间线ID');
            $table->unsignedInteger('stage_id')->comment('阶段ID');
            $table->unsignedInteger('style_id')->comment('风格ID');
            $table->unsignedInteger('estate_id')->comment('小区ID');
            $table->string('title', 50)->comment('标题');
            $table->string('address', 100)->comment('地址');
            $table->string('cover', 100)->comment('封面');
            $table->unsignedTinyInteger('shi')->comment('室');
            $table->unsignedTinyInteger('ting')->comment('厅');
            $table->unsignedTinyInteger('wei')->comment('卫');
            $table->unsignedDecimal('area', 5, 2)->comment('面积');
            $table->string('fee_type', 10)->comment('费用类型：全包，半包');
            $table->unsignedDecimal('fee', 5, 2)->comment('费用');
            $table->unsignedInteger('view')->comment('浏览数');
            $table->unsignedTinyInteger('status')->comment('状态：0进行中，1完成')->default(0);
            $table->string('qrcode', 100)->comment('工地小程序二维码');
            
            $table->timestamps();
            $table->softDeletes();
            $table->comment = '工地表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
