<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultingRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consulting_records', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户ID');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedTinyInteger('type')->comment('状态：0咨询，1报价，2量房')->default(0);
            $table->string('name', 20)->comment('姓名');
            $table->string('phone', 20)->comment('手机号');
            $table->unsignedDecimal('area', 5, 2)->comment('面积')->default(0);
            $table->unsignedTinyInteger('status')->comment('状态：0待处理，1已处理')->default(0);
            $table->unsignedInteger('staff_id')->comment('员工ID（处理人）')->default(0);
            $table->string('content', 500)->comment('处理描述');

            $table->timestamps();
            $table->softDeletes();
            $table->comment = '咨询记录表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consulting_records');
    }
}
