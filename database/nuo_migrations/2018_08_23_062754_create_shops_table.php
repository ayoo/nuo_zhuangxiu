<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            
            $table->increments('id');
            $table->unsignedInteger('company_id')->comment('门店ID');
            $table->unsignedInteger('administrator_id')->comment('公司管理员ID');
            $table->unsignedDecimal('star', 3, 1)->comment('星级');
            $table->string('title', 100)->comment('门店名称');
            $table->string('logo', 100)->comment('门店logo');
            $table->string('tel', 20)->comment('门店电话');
            $table->string('after_sale_tel', 20)->comment('售后电话');
            $table->string('address', 200)->comment('门店地址');
            $table->string('intro', 500)->comment('门店介绍');
            $table->string('longitude', 20)->comment('经度');
            $table->string('latitude', 20)->comment('纬度');
            $table->string('app_id', 50);
            $table->string('app_secret', 50);
            $table->string('code', 20)->comment('门店代码');
            $table->string('qrcode', 100)->comment('门店小程序二维码');
            // $table->string('access_token', 500);
            // $table->unsignedInteger('access_token_deadline');
            $table->timestamps();
            $table->softDeletes();
            $table->comment = '装修门店表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
