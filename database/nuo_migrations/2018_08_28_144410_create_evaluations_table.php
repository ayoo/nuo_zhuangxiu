<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedInteger('site_id')->comment('工地ID');
            $table->unsignedInteger('user_id')->comment('业主ID');
            $table->unsignedDecimal('star', 3, 1)->comment('星级');
            $table->string('title', 30)->comment('标题');
            $table->string('content', 500)->comment('内容');
            $table->unsignedTinyInteger('status')->comment('状态推：0：隐藏（只有业主可见），1：显示，2：推荐到首页')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->comment = '评价表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
