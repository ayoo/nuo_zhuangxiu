<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedTinyInteger('identity')->comment('身份：0：普通，1：业主');
            $table->string('nickname', 100)->comment('微信昵称');
            $table->unsignedTinyInteger('gender');
            $table->string('province', 50);
            $table->string('country', 50);
            $table->string('city', 50);
            $table->string('avatar', 200);
            $table->string('wx_openid', 100);
            $table->string('wx_unionid', 100);
            $table->string('wx_session_key', 100);
            $table->string('phone', 20)->comment('手机');
            $table->string('email', 100)->comment('邮箱');
            $table->string('scene', 20)->comment('场景');
            $table->string('scene_content_id', 100)->comment('场景openid');
            $table->string('password', 60)->comment('密码');
            $table->string('remember_token', 100)->nullable();
            $table->timestamp('last_activity_time')->nullable()->comment('最后一次活动时间');
            $table->timestamps();
            $table->softDeletes();
            $table->comment = '业主表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
