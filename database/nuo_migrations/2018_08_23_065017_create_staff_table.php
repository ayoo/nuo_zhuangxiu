<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedTinyInteger('status')->comment('内容类型：1：正常，2：禁用')->default(1);
            $table->unsignedInteger('staff_account_id')->comment('员工账号ID');
            $table->unsignedInteger('company_id')->comment('公司ID');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedInteger('team_id')->default(0)->comment('团队ID');
            $table->unsignedSmallInteger('site_count')->default(0)->comment('当前工地数量');
            $table->unsignedSmallInteger('case_count')->default(0)->comment('案例数量');
            $table->string('realname', 20)->comment('姓名');
            $table->string('nickname', 100)->comment('微信昵称');
            $table->string('avatar', 200)->comment('头像');
            $table->string('title', 20)->comment('头衔');
            $table->string('cover', 100)->comment('封面');
            $table->unsignedTinyInteger('work_age')->default(0)->comment('工龄');
            $table->string('phone', 20)->comment('手机');
            $table->string('wechat', 20)->comment('微信账号');
            $table->string('email', 50)->comment('邮箱');
            $table->string('intro', 500)->comment('介绍');
            $table->string('code', 50)->comment('编码');
            $table->string('qrcode', 100)->comment('员工小程序二维码');
            $table->timestamp('last_view_behavior_time')->comment('最后查看行为日志时间');
            $table->timestamp('last_view_customer_time')->comment('最后一次查看客户时间');
            $table->timestamp('last_view_update_time')->comment('最后一次查看动态时间');
            $table->timestamp('last_view_evaluation_time')->comment('最后一次查看评价时间');

            $table->timestamps();
            $table->softDeletes();
            $table->comment = '员工表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
