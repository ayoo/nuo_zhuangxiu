<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('staff_id')->index();
            $table->enum('type', [
                'none',
                'article', 'article_detail', 
                'case', 'case_detail', 
                'poster', 'qualification', 'banner',
                'ad', 'product','site','update','staff'
            ])->default('none')->index();
            $table->string('url')->index();
            $table->timestamps();
            $table->softDeletes();
            $table->comment = '图片表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
