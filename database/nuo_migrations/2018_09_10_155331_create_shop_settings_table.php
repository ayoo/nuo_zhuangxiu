<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_settings', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedTinyInteger('poster_play')->comment('海报展示方式0：静态：1：自动轮播')->default(0);
            $table->unsignedTinyInteger('ad_play')->comment('广告展示方式0：静态：1：自动轮播')->default(0);
            $table->string('consulting_banner', 100)->comment('咨询页banner')->default('default/consulting_banner.png');
            $table->string('consulting_price_banner', 100)->comment('咨询价格页banner')->default('default/consulting_price_banner.png');
            $table->string('consulting_measure_banner', 100)->comment('咨询量房页banner')->default('default/consulting_measure_banner.png');
            $table->string('support_logo', 100)->comment('技术支持logo')->default('default/nuo_support.png');
            $table->string('support_url', 100)->comment('技术支持url')->default('http://nuoidea.com');
            $table->unsignedTinyInteger('use_watermark')->comment('打水印：0：关闭：1：开启')->default(1);
            $table->string('watermark_text', 20)->comment('水印文字');
            $table->unsignedTinyInteger('customer_service')->comment('客服0：关闭：1：开启')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->comment = '门店设置表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_settings');
    }
}
