<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffUserNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_user_notes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedInteger('staff_id')->comment('员工ID');
            $table->unsignedInteger('user_id')->comment('用户ID');
            $table->string('content', 200)->comment('内容');

            $table->timestamps();
            $table->softDeletes();
            $table->comment = '员工客户备注表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_user_notes');
    }
}
