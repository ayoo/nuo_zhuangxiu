<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedInteger('site_id')->comment('工地ID');
            $table->unsignedInteger('update_id')->comment('动态ID');
            $table->string('author_type', 10)->comment('作者类型');
            $table->unsignedInteger('author_id')->comment('作者ID');
            $table->string('content', 300)->comment('内容');

            $table->timestamps();
            $table->softDeletes();
            $table->comment = '动态评论表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
