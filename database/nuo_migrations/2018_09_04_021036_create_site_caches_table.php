<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteCachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_caches', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('site_id')->comment('工地ID');
            $table->unsignedInteger('follower_count')->comment('围观人数')->default(0);
            $table->unsignedInteger('update_count')->comment('更新数')->default(0);
            $table->text('followers')->comment('最新围观的N个人');
            $table->unsignedInteger('last_update_id')->comment('最新一条动态ID')->default(0);
            $table->text('last_update')->comment('最新一条动态');

            $table->timestamps();
            $table->softDeletes();
            $table->comment = '围观关注表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_caches');
    }
}
