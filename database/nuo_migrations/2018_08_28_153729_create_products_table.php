<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedTinyInteger('status')->comment('状态：0：不显示，1：显示')->dufault(1);
            $table->unsignedSmallInteger('sort')->dufault(0);
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedInteger('staff_id')->comment('员工ID');
            $table->string('title', 50)->comment('标题');
            $table->string('cover', 100)->comment('封面');
            $table->string('banner', 100)->comment('页头海报');
            $table->timestamps();
            $table->softDeletes();
            $table->comment = '产品套餐表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
