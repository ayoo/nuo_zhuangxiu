<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_accounts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->string('username', 20)->comment('登录名');
            $table->string('phone', 20)->comment('手机')->nullable();
            $table->string('email', 100)->comment('邮箱');
            $table->string('password', 60)->comment('密码');
            $table->string('remember_token', 100)->nullable();

            $table->string('realname', 20)->comment('姓名');
            $table->string('nickname', 100)->comment('微信昵称');
            $table->unsignedTinyInteger('gender');
            $table->string('province', 50);
            $table->string('country', 50);
            $table->string('city', 50);
            $table->string('avatar', 200)->comment('头像');
            $table->string('wx_openid', 100);
            $table->string('wx_unionid', 100);
            $table->string('wx_session_key', 100);
            $table->unsignedInteger('current_shop_id')->comment('当前登录的门店ID');
            
            $table->timestamps();
            $table->softDeletes();
            $table->comment = '员工帐号表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_accounts');
    }
}
