<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('updates', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedInteger('site_id')->comment('工地ID');
            $table->unsignedInteger('staff_id')->comment('员工ID');
            $table->unsignedInteger('stage_id')->comment('阶段ID');
            $table->string('content', 500)->comment('内容');

            $table->timestamps();
            $table->softDeletes();
            $table->comment = '动态表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('updates');
    }
}
