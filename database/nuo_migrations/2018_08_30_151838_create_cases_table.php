<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedTinyInteger('is_recommend')->comment('推荐首页显示：0：不推荐，1：推荐')->dufault(0);
            $table->unsignedTinyInteger('is_public')->comment('是否公用：0：不推荐，1：推荐')->dufault(0);
            $table->unsignedSmallInteger('sort')->dufault(0);
            $table->unsignedInteger('shop_id')->comment('门店ID');
            $table->unsignedInteger('staff_id')->comment('员工ID');
            $table->unsignedInteger('site_id')->comment('工地ID');
            $table->unsignedInteger('view')->comment('浏览数');
            $table->string('title', 50)->comment('标题');
            $table->string('cover', 100)->comment('封面');
            $table->unsignedTinyInteger('shi')->comment('室');
            $table->unsignedTinyInteger('ting')->comment('厅');
            $table->unsignedTinyInteger('wei')->comment('卫');
            $table->unsignedDecimal('area', 5, 2)->comment('面积');
            $table->string('fee_type', 10)->comment('费用类型：全包，半包');
            $table->unsignedDecimal('fee', 5, 2)->comment('费用');
            $table->unsignedInteger('style_id')->comment('风格ID');
            $table->timestamps();
            $table->softDeletes();
            $table->comment = '案例表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}
