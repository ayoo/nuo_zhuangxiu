<?php

namespace App\Models;

class Article extends Model
{
    public function details()
    {
        return $this->hasOne(ArticleDetail::class);
    }
}
