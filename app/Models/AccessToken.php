<?php

namespace App\Models;

class AccessToken extends Model
{
    protected $fillable = ['shop_id', 'access_token', 'access_token_deadline'];
}
