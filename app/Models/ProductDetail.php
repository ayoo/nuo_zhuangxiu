<?php

namespace App\Models;

class ProductDetail extends Model
{
    protected $casts = [ 'content' => 'array' ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
