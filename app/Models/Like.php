<?php

namespace App\Models;

class Like extends Model
{
    /**
     * 可被批量赋值的属性。
     *
     * @var array
     */
    protected $fillable = ['shop_id', 'site_id', 'update_id','author_type','author_id'];

    public function author()
    {
        return $this->morphTo()->withDefault([
            'id' => 0,
            'nickname' => '匿名',
            'avatar' => ''
        ]);
    }
}
