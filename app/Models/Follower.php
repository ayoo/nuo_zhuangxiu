<?php

namespace App\Models;

class Follower extends Model
{
    protected $fillable = ['shop_id', 'site_id', 'user_id', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
