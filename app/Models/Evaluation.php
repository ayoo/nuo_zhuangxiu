<?php

namespace App\Models;

class Evaluation extends Model
{
    protected $fillable = ['shop_id', 'site_id', 'user_id', 'star', 'content', 'is_show'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
