<?php

namespace App\Models;

class Timeline extends Model
{
    public function stages()
    {
        return $this->hasMany(Stage::class)->select('id', 'title', 'timeline_id')->orderBy('sort', 'asc');
    }
}
