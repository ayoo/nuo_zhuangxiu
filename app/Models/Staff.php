<?php

namespace App\Models;

class Staff extends Model
{
    protected $fillable = [
        'shop_id', 'team_id', 'site_count', 
        'case_count', 'name', 'title', 'avatar', 
        'work_age', 'phone', 'intro'];

    public function comments()
    {
        return $this->morphMany(Comment::class, 'author');
    }

    public function inprocessSites()
    {
        return $this->morphToMany(Site::class, 'account', 'site_members')->where('status', 0);
    }

    public function completeSites()
    {
        return $this->morphToMany(Site::class, 'account', 'site_members')->where('status', 1);
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'author');
    }

    public function account()
    {
        return $this->belongsTo(StaffAccount::class, 'staff_account_id', 'id');
    }

    public function members()
    {
        return $this->morphMany(Comment::class, 'account');
    }
}
