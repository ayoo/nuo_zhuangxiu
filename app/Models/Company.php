<?php

namespace App\Models;

class Company extends Model
{
    public function styles()
    {
        return $this->hasMany(Style::class);
    }

    public function timelines()
    {
        return $this->hasMany(Timeline::class);
    }

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }
}
