<?php

namespace App\Models;

class StaffUser extends Model
{
    protected $table = 'staff_user';

    protected $fillable = [
        'shop_id', 'staff_id', 'user_id', 'scene'
    ];
}
