<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Site extends Model
{
    protected $fillable = ['shop_id','title'];

    public function staff()
    {
        return $this->belongsToMany(Staff::class);
    }

    public function estate()
    {
        return $this->belongsTo(Estate::class);
    }

    public function updates()
    {
        return $this->hasMany(Update::class)->orderBy('id', 'desc');
    }

    public function cache()
    {
        return $this->hasOne(SiteCache::class);
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function timeline()
    {
        return $this->belongsTo(Timeline::class);
    }

    public function style()
    {
        return $this->belongsTo(Style::class);
    }

    public function followers()
    {
        return $this->hasMany(Follower::class);
    }

    public function members()
    {
        return $this->hasMany(SiteMember::class);
    }

    public function memberStaff()
    {
        return $this->morphedByMany(Staff::class, 'account', 'site_members')->withPivot('id')->whereNull('site_members.deleted_at');
    }

    public function memberUsers()
    {
        return $this->morphedByMany(User::class, 'account', 'site_members')->withPivot('id')->whereNull('site_members.deleted_at');
    }
}
