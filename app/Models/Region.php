<?php

namespace App\Models;

class Region extends Model
{
    protected $dates = [];

    public function shops()
    {
        return $this->belongsToMany(Shop::class, 'shop_region');
    }

    public function estates()
    {
        return $this->belongsToMany(Estate::class, 'shop_estate');
    }
}
