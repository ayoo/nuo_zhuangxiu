<?php

namespace App\Models;

class CaseDetail extends Model
{
    protected $casts = [ 'content' => 'array' ];

    public function case()
    {
        return $this->belongsTo(Cases::class);
    }
}
