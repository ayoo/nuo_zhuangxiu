<?php

namespace App\Models;

class FormId extends Model
{
    protected $fillable = ['shop_id', 'open_id', 'form_id', 'deadline'];
}
