<?php

namespace App\Models;

class Ad extends Model
{
    protected $fillable = ['shop_id', 'staff_id', 'title', 'cover'];

    public function details()
    {
        return $this->hasOne(AdDetail::class);
    }

    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }
}
