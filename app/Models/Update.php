<?php

namespace App\Models;

class Update extends Model
{
    protected $fillable = [];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function images()
    {
        return $this->hasMany(UpdateImage::class);
    }

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
