<?php

namespace App\Models;

class UpdateImage extends Model
{
    protected $fillable = [];

    public function oneUpdate()
    {
        return $this->belongsTo(Update::class, 'update_id', 'id');
    }
}
