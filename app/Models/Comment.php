<?php

namespace App\Models;

class Comment extends Model
{
    /**
     * 可被批量赋值的属性。
     *
     * @var array
     */
    protected $fillable = ['shop_id', 'site_id', 'update_id','author_type','author_id', 'content'];

    protected $touches = ['oneUpdate'];

    public function oneUpdate()
    {
        return $this->belongsTo(Update::class);
    }
    
    public function author()
    {
        return $this->morphTo()->withDefault(function($model){
            $model->avatar = '';
        });
    }
}
