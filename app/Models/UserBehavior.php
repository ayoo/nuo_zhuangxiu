<?php

namespace App\Models;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class UserBehavior extends Model
{
    protected $table = 'user_behaviors_';
    protected $guarded = ['id'];
    protected $casts = [ 'context' => 'array' ];

    public function __construct($tableId = '')
    {
        if($tableId) $this->table .= $tableId;

        if (! Schema::hasTable($this->table))
        {
            $this->createTable($this->table);
        }
    }

    protected function createTable($tableName)
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->increments('id');
            $table->unsignedTinyInteger('status')->comment('状态：0：隐藏，1：显示')->dufault(1);
            $table->unsignedInteger('user_id')->comment('用户ID');
            $table->string('code', 10)->comment('行为编号');
            $table->string('model', 20)->comment('模块');
            $table->string('behavior', 20)->comment('行为');
            $table->unsignedInteger('model_id')->comment('模块内容ID');
            $table->unsignedInteger('model_description')->comment('模块内容描述');
            $table->string('relation_model', 20)->comment('关联模块');
            $table->unsignedInteger('relation_model_id')->comment('关联模块内容ID');
            $table->unsignedInteger('relation_model_description')->comment('关联模块内容描述');
            $table->string('context', 500)->comment('上下文关系');
            
            $table->timestamps();

            $table->comment = '客户行为表';
        });
    }
    
}
