<?php

namespace App\Models;

class Cases extends Model
{
    protected $table = 'cases';
    protected $fillable = [];

    public function details()
    {
        return $this->hasOne(CaseDetail::class, 'case_id', 'id');
    }

    public function style()
    {
        return $this->belongsTo(Style::class);
    }
}
