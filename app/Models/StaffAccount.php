<?php

namespace App\Models;

class StaffAccount extends Model
{
    protected $fillable = ['realname', 'avatar'];
}
