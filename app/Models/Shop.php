<?php

namespace App\Models;

class Shop extends Model
{
    protected $fillable = [];

    public function sites()
    {
        return $this->hasMany(Site::class);
    }

    public function estates()
    {
        return $this->belongsToMany(Estate::class, 'shop_estate')->withPivot('site_count')->orderBy('pivot_site_count', 'desc');;
    }

    public function regions()
    {
        return $this->belongsToMany(Region::class, 'shop_region');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function posters()
    {
        return $this->hasMany(Poster::class);
    }

    public function qualifications()
    {
        return $this->hasMany(Qualification::class);
    }

    public function article()
    {
        return $this->hasOne(Article::class);
    }

    public function settings()
    {
        return $this->hasOne(ShopSetting::class)->withDefault();
    }
}
