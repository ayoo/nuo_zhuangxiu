<?php

namespace App\Models;

class Product extends Model
{
    protected $fillable = ['shop_id', 'staff_id', 'title', 'cover'];

    public function details()
    {
        return $this->hasOne(ProductDetail::class);
    }

    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }
}
