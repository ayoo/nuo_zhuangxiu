<?php

namespace App\Models;

class Team extends Model
{
    protected $fillable = ['shop_id', 'is_recommend', 'title'];

    public function staff()
    {
        return $this->hasMany(Staff::class);
    }

    public function staffLimitFive()
    {
        return $this->hasMany(Staff::class)->limit(5);
    }
}
