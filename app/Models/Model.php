<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Model extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * 查询指定装修公司.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $shopId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfShop($query, $shopId)
    {
        return $query->where('shop_id', $shopId);
    }

    /**
     * 按照创建时间排序
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $shopId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRecent($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeUdpateRecent($query)
    {
        return $query->orderBy('updated_at', 'desc');
    }

    public function scopeSort($query)
    {
        return $query->orderBy('sort', 'asc');
    }

    public function scopePage($query, $perPage = 5)
    {
        return $query->paginate($perPage);
    }
}
