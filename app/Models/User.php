<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_id', 
        'nickname', 
        'gender', 
        'city', 
        'province', 
        'country', 
        'email', 
        'password', 
        'avatar', 
        'wx_openid', 
        'wx_unionid', 
        'wx_session_key',
        'scene', 
        'scene_content_id',
        'last_activity_time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'author');
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'author');
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    // public function members()
    // {
    //     return $this->morphMany(Comment::class, 'account');
    // }
}
