<?php

namespace App\Models;

class SiteCache extends Model
{
    protected $fillable =['site_id', 'last_update', 'update_count', 'followers', 'follower_count'];

    protected $casts = [
        'followers' => 'array',
        'last_update' => 'array'
    ];
}
