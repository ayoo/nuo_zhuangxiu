<?php

namespace App\Models;

class SiteMember extends Model
{
    /**
     * 可被批量赋值的属性。
     *
     * @var array
     */
    protected $fillable = ['site_id','account_type','account_id'];

    public function account()
    {
        return $this->morphTo();
    }
}
