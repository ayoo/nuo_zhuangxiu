<?php

namespace App\Models;

class Estate extends Model
{
    protected $fillable = ['staff_id', 'shop_id', 'region_id', 'title', 'address', 'longitude', 'latitude'];
    protected $hidden = ['pivot'];

    public function sites()
    {
        return $this->hasMany(Site::class);
    }

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }

    public function regions()
    {
        return $this->belongsToMany(Region::class, 'shop_estate');
    }
}
