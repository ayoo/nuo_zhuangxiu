<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Staff;
use App\Models\StaffUser;
use App\Models\Team;
use App\Models\Site;
use App\Models\Shop;
use App\Transformers\V1\StaffTransformer;
use App\Libraries\WeChat\MiniProgram;
use App\Jobs\SendNotice;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($teamId)
    {
        $staff = Staff::with([
            'inprocessSites' => function($query){
                $query->select(['sites.id']);
            },
            'completeSites' => function($query){
                $query->select(['sites.id']);
            }
        ])->where('team_id', $teamId)->where('shop_id', $this->user()->shop_id)->get();
        return $this->collection($staff, new StaffTransformer);
    }

    /**
     * Display a listing of the site
     * 
     * @return \Illuminate\Http\Response
     */
    public function siteIndex($id)
    {
        $site = Site::where('id', $id)->ofShop($this->user()->shop_id)->first();
        if(!$site) {
            return $this->response->errorNotFound('未找到匹配的工地');
        }

        $staff = $site->staff()->get();
        return $this->collection($staff, new StaffTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function staffStore(Request $request, $code)
    {
        $status = 0;
        $wxcode = $request->header('x-wx-code', '');
        $encryptedData = $request->header('x-wx-encrypted-data', '');
        $iv = $request->header('x-wx-iv', '');
        $scene = isset($request->scene) ? $request->scene : '';

        $staff = Staff::withTrashed()->ofShop($this->user()->shop_id)->where('code', $code)->first();
        if(!$staff) {
            return $this->response->errorNotFound('员工不存在');
        }

        $shop = Shop::findOrFail($this->user()->shop_id);

        if($wxcode) {
            $miniProgram = new MiniProgram();
            $miniProgram->appId = $shop->app_id;
            $miniProgram->appSecret = $shop->app_secret;
            $loginInfo = $miniProgram->login($wxcode);
            if(!isset($loginInfo['openid'])) {
                return $this->response->array([
                    'message'     => '获取Token失败',
                    'status_code' => 422
                ])->setStatusCode(422);
            }
    
            $phoneInfo = $miniProgram->getUserData($encryptedData, $iv);
            $phone = $phoneInfo->purePhoneNumber;
    
            $this->user()->phone = $phone;
            $this->user()->wx_session_key = $loginInfo['session_key'];
            $this->user()->save();
        }
        
        $customer = StaffUser::where('staff_id', $staff->id)
                        ->where('user_id', $this->user()->id)
                        ->first();
        
        if(!$customer) {
            $result = StaffUser::Create([
                'staff_id' => $staff->id, 
                'user_id' => $this->user()->id, 
                'shop_id' => $this->user()->shop_id, 
                'scene' => $scene
            ]);

            if(!$result) {
                return $this->response->error('新建失败', 422);
            }

            SendNotice::dispatch([
                'type' => 'b_new_customer',
                'open_id' => $staff->account->wx_openid,
                'shop_id' => $this->user()->shop_id,
                'name' => $this->user()->nickname,
                'time' => date('Y-m-d H:i:s', time()),
                'phone' => $this->user()->phone,
                'shop' => $shop->title
            ])->delay(now()->addSeconds(5));

            $status = 1;
        }
        // $result = StaffUser::firstOrCreate(
        //     ['staff_id' => $staff->id, 'user_id' => $this->user()->id],
        //     ['shop_id' => $this->user()->shop_id, 'scene' => $scene]
        // );

        return $this->response->array(['status' => $status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff = Staff::where('id', $id)->ofShop($this->user()->shop_id)->get()->first();
        if(!$staff) {
            return $this->errorNotFound('未找到相应员工信息');
        }
        return $this->item($staff, new StaffTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
