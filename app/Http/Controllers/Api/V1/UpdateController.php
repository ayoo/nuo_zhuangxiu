<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Staff;
use App\Models\Update;
use App\Models\SiteCache;
use App\Models\Site;
use App\Transformers\V1\UpdateTransformer;

class UpdateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Update $update, $id)
    {
        //$site = Site::ofShop($this->user()->shop_id)->findOrFail($id);

        $stage = (int)$request->stage;

        $query = $update->query();
        
        $query->where('site_id', $id)->where('shop_id', $this->user()->shop_id);

        if($stage > 0) {
            $query->where('stage_id', $stage);
        }

        $updates = $query->with([
            'stage','images',
            'staff' => function($query){
                $query->select('id','realname', 'avatar', 'title');
            }])->recent()->page();
        return $this->response->paginator($updates, new UpdateTransformer);
    }

    public function staffIndex(Staff $staff)
    {
        if($staff->shop_id != $this->user()->shop_id) {
            return $this->response->errorNotFound('未找到相应的工作人员');
        }

        $updates = Update::where('staff_id', $staff->id)->with([
            'stage','images','staff' => function($query){
                $query->select('id','realname', 'avatar', 'title');
            }])->recent()->limit(5)->get();
        return $this->collection($updates, new UpdateTransformer('withSite'));
    }

    public function userCommentedIndex()
    {
        $updates = Update::whereHas('comments', function($query){
            $query->where(['author_type' => 'user', 'author_id' => $this->user()->id]);
        })->selectRaw('updates.*, (select created_at from comments where update_id = updates.id and `author_type` = ? and `author_id` = ? AND `deleted_at` is NULL order by `created_at` desc limit 1) as last_comment_time', ['user', $this->user()->id])
        ->with([
            'stage','images',
            'site' => function($query) {
                $query->select('id','title');
            },
            'staff' => function($query){
                $query->select('id','realname', 'avatar', 'title');
        }])->orderBy('last_comment_time', 'desc')->page();

        return $this->response->paginator($updates, new UpdateTransformer('withSite'));
    }

    public function userLikedIndex()
    {
        $updates = Update::whereHas('likes', function($query){
            $query->where(['author_type' => 'user', 'author_id' => $this->user()->id]);
        })->selectRaw('updates.*, (select created_at from likes where update_id = updates.id and `author_type` = ? and `author_id` = ? order by `created_at` desc) as last_like_time', ['user', $this->user()->id])
        ->with([
            'stage','images',
            'site' => function($query) {
                $query->select('id','title');
            },
            'staff' => function($query){
                $query->select('id','realname', 'avatar', 'title');
        }])->orderBy('last_like_time', 'desc')->page();

        return $this->response->paginator($updates, new UpdateTransformer('withSite'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $update = Update::with([
            'staff' => function($query){
                $query->select('id','realname', 'avatar', 'title');
            },
            'images' => function($query) {
                $query->select('id', 'update_id', 'url')->limit(3);
            }
            ])->findOrFail($id, ['id', 'site_id', 'staff_id','content'])->toArray();

        $count = Site::withCount('updates')->findOrFail($update['site_id']);
        $cache = SiteCache::updateOrCreate(
            ['site_id' => 1],
            ['update_count' => $count->updates_count, 'last_update' => $update]
        );
        // return $this->item($update, new UpdateTransformer);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
