<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Update;
use App\Models\Like;

class LikeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $update_id = (int)$request->update_id;
        $site = Update::findOrFail($update_id)->site;
        if($site->shop_id != $this->user()->shop_id) {
            $this->response->errorNotFound('未找到相应的动态');
        }

        $like = Like::firstOrCreate([
            'shop_id' => $site->shop_id,
            'site_id' => $site->id,
            'update_id' => $update_id, 
            'author_type' => 'user', 
            'author_id' => $this->user()->id]);

        return $this->response->array(['id' => $like->id])->setStatusCode(201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $update_id = (int)$request->update_id;
        $like = Like::where(['update_id' => $update_id, 'author_type' => 'user', 'author_id' => $this->user()->id])
            ->withTrashed()->first();

        if($like) $like->forceDelete();
        
        return $this->response->noContent();
    }
}
