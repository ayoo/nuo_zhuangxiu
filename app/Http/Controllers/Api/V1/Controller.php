<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use Dingo\Api\Routing\Helpers;

class Controller extends BaseController
{
    use Helpers;

    public function _response($code, $msg, $data)
    {
        return $this->response->array(['code' => $code, 'msg' => $msg, 'data' => $data]);
    }
}
