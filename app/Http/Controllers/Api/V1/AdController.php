<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Ad;
use App\Transformers\V1\AdTransformer;

class AdController extends Controller
{
    /**
     * 广告列表（状态为显示，按序号升序，发布时间降序排序）
     *
     * @return Ads
     */
    public function index()
    {
        $ads = Ad::ofShop($this->user()->shop_id)->whereStatus(1)->sort()->recent()->get();
        return $this->collection($ads, new AdTransformer('',['id','cover']));
    }

    /**
     * 广告详情（）
     *
     * @param  int  $id
     * @return Ad
     */
    public function show($id)
    {
        $ad = Ad::where('id', $id)->ofShop($this->user()->shop_id)->first();
        if(!$ad) {
            return $this->response->errorNotFound('未找到匹配的广告');
        }
        return $this->item($ad, new AdTransformer);
    }
}
