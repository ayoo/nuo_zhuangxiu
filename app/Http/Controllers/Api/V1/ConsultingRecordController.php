<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\ConsultingRecord;
use App\Models\Staff;
use App\Models\StaffUser;
use App\Transformers\V1\ConsultingRecordTransformer;
use App\Http\Requests\V1\ConsultingRecordRequest;
use App\Jobs\SendNotice;

class ConsultingRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConsultingRecordRequest $request, ConsultingRecord $record)
    {
        $c_type = ['免费咨询', '免费设计', '免费报价'];

        $record->user_id = $this->user()->id;
        $record->shop_id = $this->user()->shop_id;
        $record->type = $request->type;
        $record->name = $request->name;
        $record->phone = $request->phone;
        if($record->type == 1) {
            $record->area = $request->area;
        }
        $result = $record->save();

        if(!$result) {
            return $this->response->error('提交失败', 422);
        }

        $shop = $this->user()->shop;

        $staff = StaffUser::join('staff', 'staff_user.staff_id', '=', 'staff.id')
                                ->join('staff_accounts', 'staff_accounts.id', '=', 'staff.staff_account_id')
                                ->where('staff_user.user_id', $this->user()->id)
                                ->get(['staff.id','staff_accounts.wx_openid']);

        if(!$staff) {
            $staff = Staff::join('model_has_roles', 'staff.id', '=', 'model_has_roles.model_id')
                            ->join('staff_accounts', 'staff_accounts.id', '=', 'staff.staff_account_id')
                            ->where('staff.shop_id', $this->user()->shop_id)
                            ->where('model_has_roles.role_id', 2)
                            ->get(['staff.id','staff_accounts.wx_openid', 'model_has_roles.role_id']);
        }

        if($staff) {
            foreach($staff as $v) {
                SendNotice::dispatch([
                    'type' => 'b_new_consulting',
                    'open_id' => $v->wx_openid,
                    'me' => isset($v['role_id']) ? 1 : 0,
                    'name' => $record->name,
                    'phone' => $record->phone,
                    'c_type' => $c_type[$record->type],
                    'time' => date('Y-m-d H:i:s', time()),
                    'shop_id' => $shop->id,
                    'shop' => $shop->title
                ])->delay(now()->addSeconds(5));
            }
        }

        return $this->response->array(['id' => $record->id])->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
