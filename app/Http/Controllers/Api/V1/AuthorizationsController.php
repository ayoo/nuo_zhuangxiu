<?php

namespace App\Http\Controllers\Api\V1;

use Auth;
use App\Models\User;
use App\Models\Shop;
use Illuminate\Http\Request;
use App\Http\Requests\V1\AuthorizationsRequest;
use App\Libraries\WeChat\MiniProgram;

class AuthorizationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * 新微信用户注册（记录场景值），老微信用户更新相关微信数据
     *
     * @param Request $request
     * @param Shop $shop
     * @return JWT-Token
     */
    public function store(Request $request, Shop $shop)
    {
        $code = $request->header('x-wx-code', '');
        $encryptedData = $request->header('x-wx-encrypted-data', '');
        $iv = $request->header('x-wx-iv', '');
        $scene = isset($request->scene) ? $request->scene : ''; 


        $miniProgram = new MiniProgram();
        $miniProgram->appId = $shop->app_id;
        $miniProgram->appSecret = $shop->app_secret;

        $loginInfo = $miniProgram->login($code);
        if(!isset($loginInfo['openid'])) {
            return $this->response->array([
                'message'     => '获取Token失败',
                'status_code' => 422
            ])->setStatusCode(422);
        }

        $userInfo = $miniProgram->getUserData($encryptedData, $iv);
        $user = User::where(['wx_openid' => $loginInfo['openid'], 'shop_id' => $shop->id])->get()->first();
        if(!$user) {

            $sceneContentId = '';
            if($request->data && $request->iv) {
                $sceneInfo = $miniProgram->getUserData($request->data, $request->iv);
                $sceneContentId = $sceneInfo->openGId;
            }

            $user = User::create([
                'shop_id' => $shop->id,
                'nickname' => $userInfo->nickName,
                'gender' => $userInfo->gender,
                'city' => $userInfo->city,
                'province' => $userInfo->province,
                'country' => $userInfo->country,
                'avatar' => $userInfo->avatarUrl,
                'wx_openid' => $userInfo->openId,
                'wx_session_key' => $loginInfo['session_key'],
                'scene' => $scene,
                'scene_content_id' => $sceneContentId
            ]);
        } else {
            //TODO:更新微信用户信息
            $updateData = [
                'wx_session_key' => $loginInfo['session_key']
            ];

            array_map(function($dbValue, $wxValue) use ($user, $userInfo, &$updateData){
                if($user->$dbValue != $userInfo->$wxValue) {
                    $updateData[$dbValue] = $userInfo->$wxValue;
                }
            }, ['nickname', 'avatar'],['nickName', 'avatarUrl']);
            
            $user->update($updateData);
        }

        $token = Auth::guard('api')->fromUser($user);
        return $this->response->array([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 60
        ])->setStatusCode(201);
    }

    /**
     * 刷新Token
     *
     * @return JWT-Token
     */
    public function update()
    {
        $token = Auth::guard('api')->refresh();
        return $this->respondWithToken($token);
    }

    protected function respondWithToken($token)
    {
        return $this->response->array([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 60
        ]);
    }
}
