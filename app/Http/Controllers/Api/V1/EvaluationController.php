<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Evaluation;
use App\Models\Shop;
use App\Models\Staff;
use App\Transformers\V1\EvaluationTransformer;
use App\Http\Requests\V1\EvaluationRequest;
use App\Jobs\SendNotice;
class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $evaluations = Evaluation::whereIn('status', [1,2])
                    ->ofShop($this->user()->shop_id)->recent()->page();
        return $this->response->paginator($evaluations, new EvaluationTransformer);
    }

    public function recommendIndex()
    {
        $evaluations = Evaluation::where('status', 2)->ofShop($this->user()->shop_id)->recent()->limit(5)->get();
        return $this->collection($evaluations, new EvaluationTransformer);
    }

    public function siteIndex($id)
    {
        $evaluations = Evaluation::ofShop($this->user()->shop_id)->where('site_id',$id)->recent()->get();

        if(!$evaluations) {
            return $this->response->errorNotFound('没有评价');
        }
        return $this->collection($evaluations, new EvaluationTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EvaluationRequest $request, Evaluation $evaluations)
    {
        $evaluations->shop_id = $this->user()->shop_id;
        $evaluations->site_id = $request->site_id;
        $evaluations->user_id = $this->user()->id;
        $evaluations->star = $request->star;
        $evaluations->title = $request->title;
        $evaluations->content = $request->content;
        $result = $evaluations->save();

        if($result) {
            $star =  $evaluations->query()->where('shop_id', $this->user()->shop_id)->where('status', '>', '0')->avg('star');
            Shop::where('id', $this->user()->shop_id)->update(['star' => $star]);

            //店长
            $staff = Staff::join('model_has_roles', 'staff.id', '=', 'model_has_roles.model_id')
                            ->join('staff_accounts', 'staff_accounts.id', '=', 'staff.staff_account_id')
                            ->where('staff.shop_id', $this->user()->shop_id)
                            ->where('model_has_roles.role_id', 2)
                            ->get(['staff.id','staff_accounts.wx_openid', 'model_has_roles.role_id']);

            if($staff) {
                $stars = '★★★★★';

                $shop = $evaluations->shop;
                $site = $evaluations->site;

                foreach($staff as $v) {
                    SendNotice::dispatch([
                        'type' => 'b_new_evaluation',
                        'open_id' => $v->wx_openid,
                        'star' => mb_substr($stars, 0, $evaluations->star),
                        'name' => $this->user()->nickname,
                        'time' => date('Y-m-d H:i:s', time()),
                        'content' => mb_substr($evaluations->content, 0, 10).'...',
                        'shop_id' => $shop->id,
                        'site' => $site->title
                    ])->delay(now()->addSeconds(5));
                }
            }
        }
        
        
        return $this->response->created();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
