<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\FormId;
use App\Http\Requests\V1\FormIdRequest;
use Carbon\Carbon;

class FormIdController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormIdRequest $request)
    {
        $result = true;

        $formIds = $request->form_id;

        foreach($formIds as $v) {
            try {
                $result = FormId::create([
                    'shop_id' => $this->user()->shop_id,
                    'open_id' => $this->user()->wx_openid,
                    'form_id' => $v,
                    'deadline' => strtotime(Carbon::now()->addDays(6)->addHours(20))
                ]);

                if(!$result) {
                    throw new \Exception('新增失败');
                }
            } catch(\Exception $ex) {
                report($ex);
                $result = false;
                break;
            }
        }
        
        if(!$result) {
            return $this->response->error('新增失败', 422);
        }

        return $this->response->created();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
