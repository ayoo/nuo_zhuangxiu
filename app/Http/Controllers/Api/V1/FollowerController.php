<?php   

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Site;
use App\Models\Follower;

class FollowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function siteIndex($id)
    {
        $followers = ['list_0' => [], 'list_1' => []];
        Follower::select('user_id', 'status')
            ->where(['site_id' => $id, 'shop_id' => $this->user()->shop_id])
            ->with(['user' => function($query){
                $query->select('id','nickname', 'avatar');
            }])->recent()->chunk(100, function ($result) use (&$followers) {
                foreach ($result as $follower) {
                    if($follower->status == 0 || $follower->status == 1) {
                        $followers['list_'.$follower->status][] = [
                            'nickname' => $follower->user->nickname,
                            'avatar' => $follower->user->avatar
                        ];
                    }
                }
            });

        return $this->response->array($followers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $follower = Follower::where(['site_id' => $id, 'user_id' => $this->user()->id])->first();
        if($follower) {
            return $this->response->array(['status' => $follower->status]);
        } else {
            return $this->response->errorNotFound('没有找到相应工地关注情况');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

    }

    public function follow($id)
    {
        $site = Site::ofShop($this->user()->shop_id)->findOrFail($id);

        $result = Follower::updateOrCreate(
            ['site_id' => $site->id, 'user_id' => $this->user()->id, 'shop_id' => $site->shop_id],
            ['status' => 1]
        );

        if($result) {
            return $this->response->noContent();
        } else {
            throw new Dingo\Api\Exception\UpdateResourceFailedException('关注失败');
        }
    }

    public function unfollow($id)
    {
        $site = Site::ofShop($this->user()->shop_id)->findOrFail($id);

        $result = Follower::updateOrCreate(
            ['site_id' => $site->id, 'user_id' => $this->user()->id, 'shop_id' => $site->shop_id],
            ['status' => 0]
        );

        if($result) {
            return $this->response->noContent();
        } else {
            throw new Dingo\Api\Exception\UpdateResourceFailedException('取消关注失败');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
