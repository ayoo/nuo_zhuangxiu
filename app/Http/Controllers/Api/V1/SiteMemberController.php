<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Site;
use App\Models\SiteMember;
use App\Transformers\V1\SiteTransformer;
use App\Transformers\V1\SiteMemberTransformer;

class SiteMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $site = Site::ofShop($this->user()->shop_id)->findOrFail($id);

        $members = [];

        $staff = $site->memberStaff()->with([
            'inprocessSites' => function($query){
                $query->select(['sites.id']);
            },
            'completeSites' => function($query){
                $query->select(['sites.id']);
            }
        ])->get();

        if($staff) {
            foreach($staff as $v) {
                $members[] = [
                    'id' => $v->pivot->id,
                    'account_type' => $v->pivot->account_type,
                    'account' => [
                        'id' => $v->id,
                        'avatar' => $v->avatar,
                        'realname' => $v->realname,
                        'title' => $v->title,
                        'work_age' => $v->work_age,
                        'site_count' => $v->inprocessSites->count(),
                        'case_count' => $v->completeSites->count()
                    ]
                ];
            }
        }

        $users =  $site->memberUsers()->get();

        if($users) {
            foreach($users as $v) {
                $members[] = [
                    'id' => $v->pivot->id,
                    'account_type' => $v->pivot->account_type,
                    'account' => [
                        'id' => $v->id,
                        'avatar' => $v->avatar,
                        'realname' => $v->nickname,
                        'title' => '业主'
                    ]
                ];
            }
        }

        return $this->response->array(['data' => $members]);
        //return $this->collection($members, new SiteMemberTransformer);
    }

    public function userIndex(Site $site)
    {
        //$query = $site->query();

        $sites = Site::select('sites.*')->join('site_members', 'sites.id', '=', 'site_members.site_id')
                    ->where(['site_members.account_id' => $this->user()->id, 'site_members.account_type' => 'user'])
                    ->whereNull('site_members.deleted_at')
                    ->with(['members' => function($query){$query->where('account_type', 'staff');}], 'members.account')
                    ->get();

        return $this->collection($sites, new SiteTransformer('with_members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
