<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserBehavior;
use App\Http\Requests\V1\UserBehaviorRequest;
use Illuminate\Support\Facades\DB;

class UserBehaviorController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserBehaviorRequest $request)
    {
        
        $data = $request->all();

        if($request->code == '0901') {
            $context = ['estate' => '0918', 'style' => '0919', 'stage' => '0920'];
            foreach($context as $k => $v) {
                $context_id = $k.'_id';
                $context_title = $k.'_title';
    
                if($request->$context_id && $request->$context_title) {
                    $data['context'][$k] = [
                        'id' => (int) $request->$context_id,
                        'title' => $request->$context_title
                    ];
    
                    $this->saveBehavior([
                        'status' => 0,
                        'code' => $v,
                        'relation_model' => $k,
                        'relation_model_id' => $data['context'][$k]['id'],
                        'relation_model_description' => $data['context'][$k]['title'],
                        'context' => [$k => $data['context'][$k]]
                    ]);
                }
            }
        } else if($request->code == '0915') {
            if($request->stage_id && $request->stage_title) {
                $data['relation_model'] = 'stage';
                $data['relation_model_id'] = (int)$request->stage_id;
                $data['relation_model_description'] = $request->stage_title;
                $data['context']['stage'] = [
                    'id' => (int)$request->stage_id,
                    'title' => $request->stage_title
                ];
            }
        } else if(in_array($request->code, ['0910','0911'])) {
            
            $data['relation_model'] = 'update';
            $data['relation_model_id'] = (int)$request->update_id;

            $data['context'] = [
                'update_id' => (int)$request->update_id,
                'comment_id' => (int)$request->comment_id
            ];
        } else if(in_array($request->code, ['0912','0913'])) {
            
            $data['relation_model'] = 'update';
            $data['relation_model_id'] = (int)$request->update_id;

            $data['context'] = [
                'update_id' => (int)$request->update_id,
                'like_id' => (int)$request->like_id
            ];
        }

        $result = $this->saveBehavior($data);
        if(!$result) {
            return $this->response->error('Create Failed', 422);
        }

        $result = User::where('id', $this->user()->id)
                            ->update(['last_activity_time' => date('Y-m-d H:i:s', time())]);
        if(!$result) {
            return $this->response->error('Create Failed', 422);
        }

        return $this->response->created();
    }

    protected function saveBehavior($data)
    {
        $modelBehavior = config('behavior.model_behavior');

        try {

            if(!isset($data['code']) ||  !isset($modelBehavior[$data['code']])) {
                throw new \Exception('Code error.');
            }
            $behavior = new UserBehavior($this->user()->shop_id);
            
            $behavior->status = $data['status'] ??  1;
            $behavior->user_id = $this->user()->id;
            $behavior->code = $data['code'];
            $behavior->model = $modelBehavior[$data['code']]['m'];
            $behavior->behavior = $modelBehavior[$data['code']]['b'];
            $behavior->model_id = $data['model_id'] ?? 0;
            $behavior->model_description = $data['model_description'] ?? '';
            $behavior->relation_model = $data['relation_model'] ?? '';
            $behavior->relation_model_id = $data['relation_model_id'] ?? 0;
            $behavior->relation_model_description = $data['relation_model_description'] ?? '';
            if(isset($data['context']) && !empty($data['context'])) {
                $behavior->context = $data['context'];
            }

            $result = $behavior->save();
            if(!$result) {
                throw new \Exception('Create Failed.');
            }

        } catch(\Exception $ex) {
            report($ex);
            return false;
        }

        return true;
    }
}
