<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Transformers\V1\ArticleTransformer;

class ArticleController extends Controller
{
    /**
     * 关于门店-显示
     *
     * @return Article
     */
    public function shopShow()
    {
        $article = Article::where('shop_id', $this->user()->shop_id)->first();
        if(!$article) {
            return $this->response->errorNotFound('未找到相应文章！');
        }
        return $this->item($article, new ArticleTransformer);
    }
}
