<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Site;
use App\Models\Follower;
use App\Models\SiteCache;
use App\Transformers\V1\SiteTransformer;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Site $site)
    {
        $query = $site->query();

        $estate = (int)$request->estate;
        $stage = (int)$request->stage;
        $style = (int)$request->style;
        $ai = (int)$request->ai;


        if($estate > 0) {
            $query->where('sites.estate_id', $estate);
        }

        if($stage > 0) {
            $query->where('sites.stage_id', $stage);
        }

        if($style > 0) {
            $query->where('sites.style_id', $style);
        }

        //我的装修
        if($ai == 1) {
            $query->join('site_members', 'sites.id', '=', 'site_members.site_id')
            ->where(['site_members.account_id' => $this->user()->id, 'site_members.account_type' => 'user']);
        }

        //我关注的
        if($ai == 2) {
            $query->join('followers', 'sites.id', '=', 'followers.site_id')
            ->where('followers.user_id' , $this->user()->id)
            ->where('followers.status', 1);
        }

        $sites = $query->select('sites.*')
                    ->with('cache', 'stage', 'estate')
                    ->where('sites.shop_id', $this->user()->shop_id)
                    ->orderBy('sites.updated_at', 'desc')
                    ->page();

        return $this->response->paginator($sites, new SiteTransformer('with_cache'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Site::create(['title' => $request->title, 'shop_id' => 1]);
        return ['code' => 1];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $site = Site::ofShop($this->user()->shop_id)->findOrFail($id);

        $site->increment('view', 1);

        $follower = Follower::firstOrNew(
            ['user_id' => $this->user()->id, 'site_id' => $site->id],
            ['shop_id' => $site->shop_id, 'site_id' => $site->id, 'user_id' => $this->user()->id]
        );

        $avatars = [];
        $follower_count = 0;

        if(!$follower->exists) {
            //保存围观记录
            $follower->save();

            //最近7个围观用户头像
            $avatars = $this->getlatestFollowerAvatars($site);
            //围观人数
            $follower_count = Follower::where('site_id', $site->id)->count();

            $cache = SiteCache::updateOrCreate(
                ['site_id' => $site->id],
                ['follower_count' => $follower_count, 'followers' => $avatars]
            );
        }

        if(empty($avatars)) {
            //最近7个围观用户头像
            $avatars = $this->getlatestFollowerAvatars($site);
            //围观人数
            $follower_count = Follower::where('site_id', $site->id)->count();
        }

        $site['followers'] = $avatars;
        $site['follower_count'] = $follower_count;

        return $this->item($site, new SiteTransformer('without_cache'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $site = Site::find($id);
        $site->delete();
        if($site->trashed()){
            echo '软删除成功！';
            dd($site);
        }else{
            echo '软删除失败！';
        }
        // Site::where('id', $id)->delete();
        // return ['code' => 1];
        // if (Site::trashed()) {
        //     echo 1;
        // } else {
        //     echo 2;
        // }
    }

    protected function getlatestFollowerAvatars(Site $site)
    {
        $avatars = [];

        $followers = Follower::with(['user' => function($query){
            $query->select('id','avatar');
        }])->where('site_id', $site->id)->orderBy('id', 'desc')->limit(7)->get()->toArray();

        if($followers) {
            foreach($followers as $v) {
                $avatars[] = $v['user']['avatar'];
            }
        }

        return $avatars;
    }
}
