<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Cases;
use App\Transformers\V1\CaseTransformer;

class CaseController extends Controller
{
    /**
     * 精选案例列表（按序号升序排序，按发布时间降序排序，分页）
     * 1. 全部
     * 2. 某员工发布的案例（自己发布的精选哪里，设置为公共的精选案例）
     *
     * @param Request $request
     * @param Cases $case
     * @return Case
     */
    public function index(Request $request, Cases $case)
    {
        $query = $case->query();
        //指定装修公司ID
        $query->ofShop($this->user()->shop_id);
        //是否指定员工
        if($staffId = (int)$request->staff_id) {
            $query->whereRaw('(staff_id = ? OR is_public = 1)', [$staffId]);
        }
        $cases = $query->with('style')->sort()->recent()->page();

        return $this->response->paginator($cases, new CaseTransformer);
    }

    /**
     * 精选案例推荐列表（按序号升序排序，按发布时间降序排序，前5条）
     *
     * @return Case
     */
    public function recommendIndex()
    {
        $cases  = Cases::ofShop($this->user()->shop_id)->where('is_recommend', 1)->sort()->recent()->take(5)->get();
        return $this->collection($cases, new CaseTransformer);
    }

    /**
     * 精选案例详情
     *
     * @param  int  $id
     * @return Case
     */
    public function show($id)
    {
        $case = Cases::where('id', $id)->ofShop($this->user()->shop_id)->first();
        if(!$case) {
            return $this->response->errorNotFound('未找到匹配的案例');
        }
        return $this->item($case, new CaseTransformer);
    }
}
