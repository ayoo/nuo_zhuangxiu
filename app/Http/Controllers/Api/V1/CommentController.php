<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Update;
use App\Transformers\V1\CommentTransformer;
use App\Http\Requests\V1\CommentRequest;

class CommentController extends Controller
{
    /**
     * 发布评论
     *
     * @param CommentRequest $request
     * @param Comment $comment
     * @return void
     */
    public function store(CommentRequest $request, Comment $comment)
    {
        //TODO:换成插入中间表的方式。
        $site = Update::findOrFail($request->update_id)->site;
        if($site->shop_id != $this->user()->shop_id) {
            $this->response->errorNotFound('未找到相应的动态');
        }

        $comment->shop_id = $site->shop_id;
        $comment->site_id = $site->id;
        $comment->update_id = $request->update_id;
        $comment->content = $request->content;
        $comment->author_type = 'user';
        $comment->author_id = $this->user()->id;

        $comment->save();
        
        return $this->response->array(['id' => $comment->id])->setStatusCode(201);
    }

    /**
     * 删除评论
     *
     * @param int $id
     * @return void
     */
    public function destroy($id)
    {
        $Comment = Comment::where(['id' => $id, 'author_type' => 'user', 'author_id' => $this->user()->id])->delete();
        return $this->response->noContent();
    }
}
