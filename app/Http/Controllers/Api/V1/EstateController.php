<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Estate;
use App\Models\Site;
use App\Models\Shop;
use App\Models\Region;
use App\Transformers\V1\EstateTransformer;
use Illuminate\Support\Facades\DB;

class EstateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Shop $shop)
    {
        $estates = $shop->estates();
        $this->collection($estates, new EstateTransformer('with_site_count'));
    }

    public function regionIndex(Region $region)
    {
        $estates = $region->estates($this->user()->shop_id)->get();
        $this->collection($estates, new EstateTransformer);
    }

    public function shopIndex(Request $request)
    {
        $type = $request->t ?? 'c';
        
        switch ($type) {
            case 'c':
                return $this->shopIndexOrderByCount();
            case 'd':
                return $this->shopIndexOrderByDistance($request);
            default:
                return $this->shopIndexOrderByCount();
        }
    }

    protected function shopIndexOrderByCount()
    {
        $shop = Shop::findOrFail($this->user()->shop_id);
        $estates = $shop->estates()->limit(3)->get();
        return $this->response->collection($estates, new EstateTransformer('with_site_count'));
    }

    protected function shopIndexOrderByDistance(Request $request)
    {
        $shop = Shop::findOrFail($this->user()->shop_id);
        $estates = $shop->estates;

        $longitude = $request->lng ?? 0;
        $latitude = $request->lat ?? 0;

        $distanceArray = [];
        $estatesArray = [];

        foreach($estates as $k => $v) {
            $distance = getDistance($v['latitude'], $v['longitude'], $latitude, $longitude);
            $distanceArray[] = $distance;
            $estatesArray[] = [
                'id' => (int)$v['id'],
                'title' => $v['title'],
                'address' => $v['address'],
                'longitude' => (float)$v['longitude'],
                'latitude' => (float)$v['latitude'],
                'site_count' => (int)$v['pivot']['site_count'],
                'distance' => (float)$distance
            ];
        }

        array_multisort($distanceArray, SORT_ASC, $estatesArray);

        return $this->response->array(['data' => $estatesArray]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
