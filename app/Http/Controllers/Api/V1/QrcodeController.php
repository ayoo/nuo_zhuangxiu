<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Handlers\ImageUploadHandler;
use App\Libraries\WeChat\Qrcode;

class QrcodeController extends Controller
{
    public function show(Request $request)
    {
        $qrcode = '';

        $types = config('wechat.qrcode');

        $type = $request->type;
        if(!isset($types[$type])) {
            return $this->response->error('类型错误', 422);
        }

        $id = (int)$request->id;
        if($type == 'home') {
            $id = $this->user()->shop_id;
        }

        $incetance = app($types['repo'][$type]);

        $object = $incetance->findOrFail($id);
        if($object->qrcode) {
            $qrcode = $object->qrcode;
        } else {
            $qrcode = $this->generate(app(ImageUploadHandler::class), $type, [$id], $this->user()->shop_id);
            if(!$qrcode) {
                return $this->response->error('生成二维码失败', 422);
            } 

            $object->qrcode = $qrcode;
            $object->save();
        }

        return $this->response->array(['url' => imageUrl($qrcode)]);
    }

    protected function generate(ImageUploadHandler $uploader, $type, $params, $shopId)
    {
        $qrcode = new Qrcode($type, $shopId, $params);

        $imgStream = $qrcode->generate();
        if(!$imgStream) {
            return false;
        }

        $key = $uploader->uploadToDisk($imgStream, 'qrcode', 'qrcode', 'png');
        if(!$key) {
            return false;
        }

        return $key;
    }
}
