<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'update_id'  => 'required|integer',
            'content'    => 'required|string|max:200'
        ];
    }

    public function attributes()
    {
        return [
            'update_id' => '动态ID',
            'content' => '评论内容'
        ];
    }
}
