<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;
use Illuminate\Validation\Rule;

class ConsultingRecordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|string|max:20',
            //'phone' => ['required','regex:/^1(?:3\d|4[4-9]|5[0-35-9]|6[67]|7[013-8]|8\d|9\d)\d{8}$/'],
            'phone' => ['required','regex:/^1\d{10}$/'],
            'type'  => 'required|in:0,1,2',
            'area'  => 'required_if:type,1|numeric'
        ];
    }

    public function attributes()
    {
        return [
            'name'  => '姓名',
            'phone' => '手机号',
            'type'  => '咨询类型',
            'area'  => '住房面积'
        ];
    }

    public function messages()
    {
        return [
            'phone.regex' => '手机号 格式错误。',
            'type.in' => '咨询类型 错误',
            'area.required_if' => '住房面积 不能为空。'
        ];
    }
}
