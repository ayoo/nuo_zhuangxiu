<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;
use Illuminate\Validation\Rule;

class EvaluationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_id' => 'required|integer|exists:site_members,site_id,account_type,user,account_id,'.$this->user()->id,
            'title'   => 'required|string|max:30',
            'content' => 'required|string|max:500',
            'star'    => 'required|integer|in:1,2,3,4,5',
        ];
    }

    public function attributes()
    {
        return [
            'site_id'   => '工地ID',
            'title'     => '评价',
            'content'   => '内容',
            'star'      => '评分'
        ];
    }

    public function messages()
    {
        return [
            'star.in' => '评分 超出范围（1-5）。'
        ];
    }
}
