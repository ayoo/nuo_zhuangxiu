<?php

namespace App\Handlers;

// use Image;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class ImageUploadHandler
{
    protected $allowed_ext = ["png", "jpg", "gif", 'jpeg'];

    public function save($file, $folder, $file_prefix, $watermark = '')
    {
        $extension = strtolower($file->getClientOriginalExtension()) ?: 'png';

        // 如果上传的不是图片将终止操作
        if ( ! in_array($extension, $this->allowed_ext)) {
            return false;
        }

        $img = Image::make($file);

        Log::info('Mem used 1: '.$this->convert(memory_get_usage()));

        if($watermark) {
            $width = $img->width();
            $height = $img->height();

            $fontSize = intval($width / 22);
            Log::info('Width：'.$width.' Height：'.$height.' Size：'.$fontSize);
            $x = $width - 10;
            $y = $height - 10;
            $stream = $img->text($watermark, $x, $y, function($font) use ($fontSize) {
                $font->file(public_path() . '/fonts/SourceHanSansCN-Medium.otf');
                $font->size($fontSize);
                $font->color(array(255, 255, 255));
                $font->align('right');
            })->stream($extension, 100);
        } else {
            $stream = $img->stream($extension, 100);
        }

        Log::info('Mem used 2: '.$this->convert(memory_get_usage()));

        $key = $this->uploadToDisk($stream, $folder, $file_prefix, $extension);

        return ['path' => $key];
    }

    public function uploadToDisk($fileStream, $folder, $file_prefix, $extension)
    {
        $key = $this->getFileKey($folder, $file_prefix, $extension);

        try {
            $disk = Storage::disk('cos');
            $res = $disk->put($key , $fileStream);
        } catch (\Exception $ex) {
            report($ex);
            return false;
        }
        
        return $key;
    }

    public function getFileKey($folder, $file_prefix, $extension)
    {
        //文件夹
        $folder_name = "images/$folder/" . date("Ym", time()) . '/'.date("d", time());

        //文件名
        $filename = $file_prefix . '_' . time() . '_' . str_random(10) . '.' . $extension;

        //key
        $key = $folder_name . '/' . $filename;

        return $key;
    }

    protected function convert($size)
    {
        $unit=array('b','kb','mb','gb','tb','pb'); 
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i]; 
    }
}
