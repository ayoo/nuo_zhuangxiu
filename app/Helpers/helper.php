<?php
/**
 * Create image url.
 */
if (! function_exists('imageUrl')) {
    function imageUrl($path)
    {
        return $path ? config('tencentcos.domain').'/'.$path : '';
    }
}

if(! function_exists('unserializeData')) {
    function unserializeData($data)
    {
        $unserializeDate = null;

        if($data) {
            try {
                $unserializeDate = \unserialize($data);
            }catch(\Exception $e) {
                $unserializeDate = null;
            }
        }

        return $unserializeDate;
    }
}

/**
 *  Calculate distance
 */
if(! function_exists('getDistance')) {
    function getDistance($lat1, $lng1, $lat2, $lng2)
    {
        $earthRadius = 6367000; //approximate radius of earth in meters
    
        /*
        Convert these degrees to radians
        to work with the formula
        */
    
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
    
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
    
        /*
        Using the
        Haversine formula
    
        http://en.wikipedia.org/wiki/Haversine_formula
    
        calculate the distance
        */
    
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  
        $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
    
        return round($calculatedDistance);
    }
}

/**
 * Array to Object
 */
if(! function_exists('arrayToObject')) {
    function arrayToObject($arr) {
        if (gettype($arr) != 'array') {
            return;
        }
        foreach ($arr as $k => $v) {
            if (gettype($v) == 'array' || getType($v) == 'object') {
                $arr[$k] = (object)arrayToObject($v);
            }
        }
     
        return (object)$arr;
    }
}

/**
 * Object to Array
 */
if(! function_exists('objectToArray')) {
    function objectToArray($obj) {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)objectToArray($v);
            }
        }
     
        return $obj;
    }
}

if(! function_exists('xdump')) {
    function xdump($var, $echo = true, $label = null, $strict = true)
    {
        $label = ($label === null) ? '' : rtrim($label) . ' ';
        if (!$strict) {
            if (ini_get('html_errors')) {
                $output = print_r($var, true);
                $output = "<pre>" . $label . htmlspecialchars($output, ENT_QUOTES) . "</pre>";
            } else {
                $output = $label . " : " . print_r($var, true);
            }
        } else {
            ob_start();
            var_dump($var);
            $output = ob_get_clean();
            if (!extension_loaded('xdebug')) {
                $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            }
        }
        if ($echo) {
            echo($output);
            return null;
        } else {
            $output = preg_replace("/\<[\/]?pre\>/m", "", $output);
            return "\n" . htmlspecialchars_decode($output);
        }
    }
}