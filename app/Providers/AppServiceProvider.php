<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use App\Services\FileSystem\CosAdapter;
use Qcloud\Cos\Client;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Storage::extend('cos', function ($app, $config) {
            $client = new Client($config);
            return new Filesystem(new CosAdapter($client, $config));
        });

        Relation::morphMap([
            'user'  => 'App\Models\User',
            'staff' => 'App\Models\Staff',
        ]);

        Carbon::setLocale('zh');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        \API::error(function (\Illuminate\Database\Eloquent\ModelNotFoundException $exception) {
            abort('404', '您查找的数据不存在或已被删除');
        });
        \API::error(function (\Illuminate\Auth\Access\AuthorizationException $exception) {
            abort(403, $exception->getMessage());
        });
    }
}
