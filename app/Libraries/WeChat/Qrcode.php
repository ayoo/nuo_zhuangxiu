<?php
namespace App\Libraries\WeChat;

use App\Libraries\WeChat\Unit;
use App\Libraries\WeChat\AccessToken;
use Illuminate\Support\Facades\Log;

class Qrcode
{
    protected $data;
    protected $url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=';

    public function __construct($type, $shopId, $sceneParams)
    {
        $this->data = config('wechat.qrcode.'.$type);
        if(empty($this->data)) {
            throw new \Exception('Type error');
        }

        $accessToken = $this->getAccessToken($shopId);
        if(!$accessToken) {
            throw new \Exception('Get Access Token Error');
        }
        $this->url .= $accessToken;

        $this->createData($sceneParams);
    }

    public function getAccessToken($shopId)
    {
        $accessToken = new AccessToken($shopId);
        return $accessToken->getShopAccessToken();
    }

    public function createData($sceneParams) 
    {
        if(!empty($sceneParams)) {
            $this->data['scene'] = call_user_func_array('sprintf', array_merge([$this->data['scene']],$sceneParams));
        }
    }
    
    public function generate()
    {
        $res = Unit::httpRequest($this->url, json_encode($this->data), 'json');
        if(is_array($res) && isset($res['errcode'])) {
            Log::error('Generate Qrcode Error:'. ($res['errcode'] ?? '') .' && '. ($res['errmsg'] ?? ''));
            return false;
        } else {
            return $res;
        }
    }
}