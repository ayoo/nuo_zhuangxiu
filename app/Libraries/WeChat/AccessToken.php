<?php
namespace App\Libraries\WeChat;

use App\Libraries\WeChat\Unit;
use App\Models\Shop;
use App\Models\AccessToken as AccessTokenModel;
use Illuminate\Support\Facades\Log;

class AccessToken
{
    protected $shopId;
    protected $appId;
    protected $appSecret;
    protected $shopAccessToken;
    protected $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential';

    public function __construct($shopId)
    {
        $this->shopId = $shopId;

        $shop = Shop::findOrFail($shopId);
        $this->appId = $shop->app_id;
        $this->appSecret = $shop->app_secret;
        
        $result = AccessTokenModel::where('shop_id', $shopId)->first();
        $this->shopAccessToken = $result ?? null;
    }
    
    public function getShopAccessToken()
    {
        $time = time();

        if($this->shopAccessToken == null || $this->shopAccessToken->access_token_deadline < $time) {
            $token = $this->getAccessToken($this->appId, $this->appSecret);
            if(!$token) {
                return false;
            }

            $result = AccessTokenModel::updateOrCreate(
                ['shop_id' => $this->shopId],
                [
                    'access_token' => $token['access_token'], 
                    'access_token_deadline' => time() + $token['expires_in'] - 300
                ]
            );

            if(!$result) {
                return false;
            }

            $accessToken = $token['access_token'];
        } else {
            $accessToken = $this->shopAccessToken->access_token;
        }

        return $accessToken;
    }

    protected function getAccessToken($appId, $secret)
    {
        $url = $this->url . '&appid=' . $appId . '&secret=' . $secret;
        $res = Unit::httpRequest($url);

        if($res && isset($res['access_token'])) {
            return $res;   
        } else {
            Log::error('Get Access Token Error:'.$res['errcode'].' && '.$res['errmsg']);
            return false;
        }
    }
}