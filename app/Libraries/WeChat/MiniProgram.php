<?php
namespace App\Libraries\WeChat;

use App\Libraries\WeChat\Unit;
class MiniProgram
{
    private $appId;
    private $appSecret;
    private $sessionKey;
    private $code2session_url;
    
    public function __construct()
    {
        $this->code2session_url = config('wechat.miniprogram.code2session_url');
    }

    public function __set($n, $v)
    {
        $this->$n = $v;
    }

    public function __get($n)
    {
        return $this->$n;
    }

    public function login($code)
    {
        return $this->getSessionKey($code);
    }

    public function getUserData($encryptedData, $iv, $sessionKey = null)
    {
        if (empty($sessionKey)) {
            $sessionKey = $this->sessionKey;
          }
          $decryptData = \openssl_decrypt(
            base64_decode($encryptedData),
            'AES-128-CBC',
            base64_decode($sessionKey),
            OPENSSL_RAW_DATA,
            base64_decode($iv)
        );
        $userinfo = json_decode($decryptData);
        return $userinfo;
    }

    private function getSessionKey($code)
    {
        $requestParams = [
            'appid' => $this->appId,
            'secret' => $this->appSecret,
            'js_code' => $code,
            'grant_type' => 'authorization_code'
        ];

        $code2session_url = $this->code2session_url . '?' . http_build_query($requestParams);
        $userInfo = Unit::httpRequest($code2session_url);
        if(!isset($userInfo['session_key'])){
            return [
                'code' => 10000,
                'code' => '获取 session_key 失败',
            ];
        }
        $this->sessionKey = $userInfo['session_key'];
        return $userInfo;
    }
}