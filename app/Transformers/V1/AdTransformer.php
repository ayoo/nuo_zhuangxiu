<?php

namespace App\Transformers\V1;

use App\Models\Ad;

class AdTransformer extends BaseTransformer
{
    protected $availableIncludes = ['details', 'staff'];

    public function transform(Ad $ad)
    {
        return $this->returnData([
            'id' => $ad->id,
            'title' => $ad->title,
            'cover' => imageUrl($ad->cover),
            'banner' => imageUrl($ad->banner)
        ]);
    }

    public function includeDetails(Ad $ad)
    {
        return $this->item($ad->details, new AdDetailTransformer);
    }

    public function includeStaff(Ad $ad)
    {
        return $this->item($ad->staff, new StaffTransformer('', ['id', 'realname', 'avatar']));
    }
}