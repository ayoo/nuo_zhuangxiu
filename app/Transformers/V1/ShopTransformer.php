<?php

namespace App\Transformers\V1;

use App\Models\Shop;

class ShopTransformer extends BaseTransformer
{
    protected $availableIncludes = ['posters', 'qualifications'];
    protected $defaultIncludes = ['settings'];

    public function transform(Shop $shop)
    {
        return $this->returnData([
            'id' => $shop->id,
            'star' => (float)$shop->star,
            'title' => $shop->title,
            'logo' => imageUrl($shop->logo),
            'tel'   => $shop->tel,
            'after_sale_tel'   => $shop->after_sale_tel,
            'address' => $shop->address,
            'intro' => $shop->intro,
            'longitude' => $shop->longitude,
            'latitude' => $shop->latitude,
            'site_count' => $shop->sites_count
        ]);
    }

    public function includePosters(Shop $shop)
    {
        return $this->collection($shop->posters, new PosterTransformer);
    }

    public function includeQualifications(Shop $shop)
    {
        return $this->collection($shop->qualifications, new QualificationTransformer);
    }

    public function includeSettings(Shop $shop)
    {
        return $this->item($shop->settings, new ShopSettingTransformer);
    }
}