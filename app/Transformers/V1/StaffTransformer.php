<?php

namespace App\Transformers\V1;

use App\Models\Staff;

class StaffTransformer extends BaseTransformer
{
    public function transform(Staff $staff)
    {
        return $this->returnData([
            'id' => $staff->id,
            'realname' => $staff->realname,
            'avatar'  => $staff->avatar,
            'title' => $staff->title,
            'cover'  => imageUrl($staff->cover),
            'work_age' => $staff->work_age,
            'phone' => $staff->phone,
            'wechat' => $staff->wechat,
            'mail' => $staff->mail,
            'site_count' => $staff->inprocessSites->count(),
            'case_count' => $staff->completeSites->count(),
            'code' => $staff->code
        ]);
    }
}