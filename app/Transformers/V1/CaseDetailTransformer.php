<?php

namespace App\Transformers\V1;

use League\Fractal\TransformerAbstract;
use App\Models\CaseDetail;

class CaseDetailTransformer extends TransformerAbstract
{
    public function transform(CaseDetail $detail)
    {
        return [
            'data' => $detail->content ?? []
        ];
    }
}