<?php

namespace App\Transformers\V1;

use App\Models\Estate;

class EstateTransformer extends BaseTransformer
{
    protected $type;

    public function __construct($type = '')
    {
        $this->type = $type;
    }

    public function transform(Estate $estate)
    {
        switch($this->type) {
            case "with_site_count":
                return $this->returnSiteCount($estate);
            default:
                return $this->returnDefault($estate);
        }
    }

    public function returnSiteCount(Estate $estate)
    {
        return $this->returnData([
            'id' => $estate->id,
            'title' => $estate->title,
            'address' => $estate->address,
            'longitude' => $estate->longitude,
            'latitude' => $estate->latitude,
            'site_count' => $estate->pivot->site_count
        ]);
    }

    public function returnDefault(Estate $estate)
    {
        return $this->returnData([
            'id' => $estate->id,
            'title' => $estate->title,
            'longitude' => $estate->longitude,
            'latitude' => $estate->latitude,
            'address' => $estate->address 
        ]);
    }
}