<?php

namespace App\Transformers\V1;

use App\Models\Product;

class ProductTransformer extends BaseTransformer
{
    protected $availableIncludes = ['details', 'staff'];

    public function transform(Product $product)
    {
        return $this->returnData([
            'id' => $product->id,
            'title' => $product->title,
            'cover' => imageUrl($product->cover),
            'banner' => imageUrl($product->banner)
        ]);
    }

    public function includeDetails(Product $product)
    {
        return $this->item($product->details, new ProductDetailTransformer);
    }

    public function includeStaff(Product $product)
    {
        return $this->item($product->staff, new StaffTransformer('', ['id', 'realname', 'avatar']));
    }
}