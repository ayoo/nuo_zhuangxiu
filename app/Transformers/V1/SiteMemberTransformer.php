<?php

namespace App\Transformers\V1;

use App\Models\SiteMember;

class SiteMemberTransformer extends BaseTransformer
{
    // protected $defaultIncludes = ['account'];
    protected $availableIncludes = ['account'];

    public function transform(SiteMember $member)
    {
        return [
            'id' => $member->id,
            'account_type' => $member->account_type,
            'account' => $member->account
        ];
    }

    public function includeAccount(SiteMember $member)
    {
        $type = $member->account_type;
        switch($type) {
            case "user":
                $userTransformer = new UserTransformer('', ['id', 'nickname', 'avatar'], ['nickname' => 'realname']);
                $userTransformer->addParam('title', '业主');
                return $this->item($member->account, $userTransformer);
            case "staff":
                return $this->item($member->account, new StaffTransformer(
                    '', ['id', 'realname', 'avatar', 'title', 'work_age', 'site_count', 'case_count']));
        }
    }
}