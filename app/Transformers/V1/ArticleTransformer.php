<?php

namespace App\Transformers\V1;

use App\Models\Article;

class ArticleTransformer extends BaseTransformer
{
    protected $availableIncludes = ['details'];

    public function transform(Article $article)
    {
        return $this->returnData([
            'id' => $article->id,
            'title' => $article->title,
        ]);
    }

    public function includeDetails(Article $article)
    {
        return $this->item($article->details, new ArticleDetailTransformer);
    }
}