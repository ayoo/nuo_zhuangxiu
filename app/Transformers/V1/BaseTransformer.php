<?php

namespace App\Transformers\V1;

use League\Fractal\TransformerAbstract;
use App\Transformers\Traits\TransformerParams;

class BaseTransformer extends TransformerAbstract
{
    use TransformerParams;

    protected $type;
    protected $fields;
    protected $newKeys;

    public function __construct($type = '', $fields = [], $newKeys = []) 
    {
        $this->type = $type;
        $this->fields =$fields;
        $this->newKeys =$newKeys;
    }

    public function returnData($data)
    {
        $returnData = [];

        if(!empty($this->fields)) {
            foreach($this->fields as $v) {
                if(isset($data[$v])) {
                    $returnData[$v] = $data[$v];
                }
            }
        } else {
            $returnData = $data;
        }
        
        if(!empty($this->newKeys)) {
            foreach($returnData as $k => $v) {
                if(isset($this->newKeys[$k])) {
                    $returnData[$this->newKeys[$k]] = $v;
                    unset($returnData[$k]);
                }
            }
        }

        return array_merge($returnData, $this->params);
    } 
}