<?php

namespace App\Transformers\V1;

use League\Fractal\TransformerAbstract;
use App\Models\ProductDetail;

class ProductDetailTransformer extends TransformerAbstract
{
    public function transform(ProductDetail $detail)
    {
        return [
            'data' => $detail->content ?? []
        ];
    }
}