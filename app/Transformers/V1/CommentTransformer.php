<?php

namespace App\Transformers\V1;

use App\Models\Comment;

class CommentTransformer extends BaseTransformer
{
    protected $availableIncludes = ['author'];
    // protected $defaultIncludes = ['author'];

    public function transform(Comment $comment)
    {
        return $this->returnData([
            'id' => $comment->id,
            'content' => $comment->content,
            'author_type' => $comment->author_type
        ]);
    }

    public function includeAuthor(Comment $comment)
    {
        $type = $comment->author_type;
        switch($type) {
            case "user":
                $user = $comment->author;
                return $this->item($user, new UserTransformer('', ['id' , 'nickname', 'avatar'], ['nickname' => 'realname']));
            case "staff":
                return $this->item($comment->author, new StaffTransformer('', ['id', 'realname', 'avatar']));
        }
    }
}