<?php

namespace App\Transformers\V1;

use App\Models\Like;

class LikeTransformer extends BaseTransformer
{
    protected $availableIncludes = ['author'];
    // protected $defaultIncludes = ['author'];

    public function transform(Like $like)
    {
        return [
            'id' => $like->id,
            'author_type' => $like->author_type
        ];
    }

    public function includeAuthor(Like $like)
    {
        $type = $like->author_type;
        switch($type) {
            case "user":
                return $this->item($like->author, new UserTransformer('', ['id', 'nickname', 'avatar'], ['nickname' => 'realname']));
            case "staff":
                return $this->item($like->author, new StaffTransformer('', ['id', 'realname', 'avatar']));
        }
    }
}