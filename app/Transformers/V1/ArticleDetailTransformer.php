<?php

namespace App\Transformers\V1;

use App\Models\ArticleDetail;

class ArticleDetailTransformer extends BaseTransformer
{
    public function transform(ArticleDetail $detail)
    {
        return [
            'data' => $detail->content ?? []
        ];
    }
}