<?php

namespace App\Transformers\V1;

use App\Models\Qualification;

class QualificationTransformer extends BaseTransformer
{
    public function transform(Qualification $qualification)
    {
        return $this->returnData([
            'id' => $qualification->id,
            'url' => imageUrl($qualification->url)
        ]);
    }
}