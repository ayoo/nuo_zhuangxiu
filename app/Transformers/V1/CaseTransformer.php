<?php

namespace App\Transformers\V1;

use App\Models\Cases;

class CaseTransformer extends BaseTransformer
{
    protected $availableIncludes = ['details'];

    public function transform(Cases $case)
    {
        return $this->returnData([
            'id' => $case->id,
            'site_id' => $case->site_id,
            'title' => $case->title,
            'cover' => imageUrl($case->cover),
            'type' => ($case->shi ? $case->shi.'室' : '').($case->ting ? $case->ting.'厅' : '').($case->wei ? $case->wei.'卫' : ''),
            'fee' => $case->fee_type.($case->fee > 0 ? (float)$case->fee.'万' : ''),
            'style' => $case->style->title ?? '',
            'area' => (float)$case->area,
            'created_at' => $case->created_at->diffForHumans()
        ]);
    }

    public function includeDetails(Cases $case)
    {
        return $this->item($case->details, new CaseDetailTransformer);
    }
}