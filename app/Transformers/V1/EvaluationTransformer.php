<?php

namespace App\Transformers\V1;

use App\Models\Evaluation;
use App\Models\User;

class EvaluationTransformer extends BaseTransformer
{
    protected $defaultIncludes = ['user'];

    public function transform(Evaluation $evaluation)
    {
        return [
            'id' => $evaluation->id,
            'site_id' => $evaluation->site_id,
            'status'  => $evaluation->status,
            'star' => (float)$evaluation->star,
            'title' => $evaluation->title,
            'content' => $evaluation->content,
            'created_at' => $evaluation->created_at->diffForHumans()
        ];
    }

    public function includeUser(Evaluation $evaluation)
    {
        return $this->item($evaluation->user, new UserTransformer('', ['id', 'nickname', 'avatar']));
    }
}