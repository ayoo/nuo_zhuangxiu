<?php

namespace App\Transformers\V1;

use League\Fractal\TransformerAbstract;
use App\Models\AdDetail;

class AdDetailTransformer extends TransformerAbstract
{
    public function transform(AdDetail $detail)
    {
        return [
            'data' => $detail->content ?? []
        ];
    }
}