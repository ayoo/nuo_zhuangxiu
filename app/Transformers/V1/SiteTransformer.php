<?php

namespace App\Transformers\V1;

use App\Models\Site;
use \Exception;

class SiteTransformer extends BaseTransformer
{
    // protected $availableIncludes = ['updates'];

    public function transform(Site $site)
    {
        switch($this->type) {
            case 'with_cache':
                return $this->returnWithCache($site);
            case 'without_cache':
                return $this->returnWithoutCache($site);
            case 'with_members':
                return $this->returnWithMembers($site);
            default:
                return $this->returnDefault($site);
        }
    }

    public function returnWithCache(Site $site) 
    {
        $last_update = $site['cache']['last_update'];
        if($last_update) {
            unset($last_update['staff_id']);

            if(!empty($last_update['images'])) {
                $images = [];
                foreach($last_update['images'] as $v) {
                    $images[] = imageUrl($v['url']);
                }
                unset($last_update['images']);
                $last_update['images'] = $images;
            }
        }

        return $this->returnData([
            'id' => $site->id,
            'title' => $site->title,
            'view' => $site->view,
            'stage' => $site['stage']['title'],
            'updated_at' => $site->updated_at->diffForHumans(),
            'longitude' => $site['estate']['longitude'],
            'latitude' => $site['estate']['latitude'],
            'follower_count' => $site['cache']['follower_count'] ?? 0,
            'followers' => $site['cache']['followers'] ?? [],
            'update_count' => $site['cache']['update_count'] ?? 0,
            'last_update' => $last_update
        ]);
    }

    public function returnWithoutCache(Site $site) 
    {
        return $this->returnData([
            'id' => $site->id,
            'status' => $site->status,
            'title' => $site->title,
            'cover' => imageUrl($site->cover),
            'address' => $site->address,
            'stage_id' => $site->stage_id,
            'timeline' => $site->timeline->stages ?? null,
            'type' => ($site->shi ? $site->shi.'室' : '').($site->ting ? $site->ting.'厅' : '').($site->wei ? $site->wei.'卫' : ''),
            'fee' => $site->fee_type.($site->fee > 0 ? (float)$site->fee.'万' : ''),
            'style' => $site->style->title ?? null,
            'area' => (float)$site->area,
            'view' => $site->view,
            'follower_count' => $site['follower_count'],
            'followers' => $site['followers'],
            'member_count' => $site->members->count()
        ]);
    }

    public function returnWithMembers(Site $site)
    {
        $new_members = null;

        if(!$site->member) {
            foreach($site->members as $v) {
                $new_members[] = [
                    'id' => $v['account']['id'],
                    'realname' =>  $v['account']['realname'],
                    'avatar' =>  $v['account']['avatar'],
                    'phone' =>  $v['account']['phone']
                ];
            }
        }
        return $this->returnData([
            'id' => $site->id,
            'title' => $site->title,
            'address' => $site->address,
            'members' => $new_members
        ]);
    }

    public function returnDefault(Site $site)
    {
        return $this->returnData([
            'id' => $site->id,
            'status' => $site->status,
            'title' => $site->title,
            'cover' => imageUrl($site->cover),
            'address' => $site->address,
            'stage_id' => $site->stage_id,
            'type' => ($site->shi ? $site->shi.'室' : '').($site->ting ? $site->ting.'厅' : '').($site->wei ? $site->wei.'卫' : ''),
            'fee' => $site->fee_type.($site->fee ? $site->fee.'万' : ''),
            'area' => (float)$site->area,
            'view' => $site->view
        ]);
    }
}