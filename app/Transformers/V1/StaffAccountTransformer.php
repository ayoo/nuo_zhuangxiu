<?php

namespace App\Transformers\V1;

use League\Fractal\TransformerAbstract;
use App\Models\StaffAccount;

class StaffAccountTransformer extends TransformerAbstract
{
    public function transform(StaffAccount $account)
    {
        return [
            'realname' => $account->realname,
            'avatar' => $account->avatar
        ];
    }
}