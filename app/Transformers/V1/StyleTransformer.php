<?php

namespace App\Transformers\V1;

use App\Models\Style;

class StyleTransformer extends BaseTransformer
{
    public function transform(Style $style)
    {
        return $this->returnData([
            'id' => $style->id,
            'title' => $style->title
        ]);
    }
}