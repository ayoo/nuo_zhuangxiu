<?php
    
namespace App\Transformers\V1;

use App\Models\Poster;

class PosterTransformer extends BaseTransformer
{
    public function transform(Poster $poster)
    {
        return $this->returnData([
            'url' => imageUrl($poster->url)
        ]);
    }
}