<?php

namespace App\Transformers\V1;

use App\Models\Timeline;

class TimelineTransformer extends BaseTransformer
{
    public function transform(Timeline $line)
    {
        return $this->returnData([
            'id' => $line->id,
            'title' => $line->title,
            'stages' => $line->stages
        ]);
    }
}