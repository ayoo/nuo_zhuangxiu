<?php

namespace App\Transformers\V1;

use Illuminate\Support\Facades\DB;
use App\Models\User;

class UserTransformer extends BaseTransformer
{
    public function transform(User $user)
    {
        return $this->returnData([
            'id' => $user->id,
            'shop_id' => $user->shop_id,
            'identity' => (int)DB::table('site_members')->where(['account_type' => 'user', 'account_id' => $user->id])->exists(),
            'nickname' => $user->nickname,
            'phone' => $user->phone,
            'avatar' => $user->avatar
        ]);
    }
}