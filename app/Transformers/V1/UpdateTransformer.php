<?php

namespace App\Transformers\V1;

use League\Fractal\TransformerAbstract;
use App\Models\Update;

class UpdateTransformer extends TransformerAbstract
{
    protected $type = '';
    protected $availableIncludes = ['likes', 'comments'];

    public function __construct($type = '')
    {
        $this->type = $type;
    }
    

    public function transform(Update $update)
    {
        if(!empty($update['images'])) {
            $images = [];
            foreach($update['images'] as $v) {
                $images[] = imageUrl($v->url);
            }
            unset($update['images']);
            $update['images'] = $images;
        }

        $data = [
            'id' => $update->id,
            'content' => $update->content,
            'created_at' => $update->created_at->diffForHumans(),
            'stage' => $update->stage->title,
            'staff' => $update->staff,
            'images' => $update->images
        ];

        if($this->type == 'withSite') {
            $data['site'] =  $update->site;
        }

        return $data;
    }

    public function includeStaff(Update $update)
    {
        return $this->item($update->staff, new StaffTransformer);
    }

    public function includeComments(Update $update)
    {
        return $this->collection($update->comments, new CommentTransformer);
    }

    public function includeLikes(Update $update)
    {
        return $this->collection($update->likes, new LikeTransformer);
    }
}