<?php

namespace App\Transformers\V1;

use App\Models\ShopSetting;

class ShopSettingTransformer extends BaseTransformer
{
    public function transform(ShopSetting $setting)
    {
        return $this->returnData([
            'poster_play' => $setting->poster_play,
            'ad_play' => $setting->ad_play,
            'consulting_banner' => imageUrl($setting->consulting_banner),
            'consulting_price_banner' => imageUrl($setting->consulting_price_banner),
            'consulting_measure_banner' => imageUrl($setting->consulting_measure_banner),
            'support_logo' => imageUrl($setting->support_logo),
            'support_url' => $setting->support_url,
            // 'use_watermark' => $setting->use_watermark,
            // 'watermark_text' => $setting->watermark_text,
            'customer_service' => $setting->customer_service
        ]);
    }
}