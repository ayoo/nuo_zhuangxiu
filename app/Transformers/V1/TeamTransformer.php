<?php

namespace App\Transformers\V1;

use League\Fractal\TransformerAbstract;
use App\Models\Team;

class TeamTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['staff'];

    public function transform(Team $team)
    {
        return [
            'id' => $team->id,
            'title' => $team->title
        ];
    }

    public function includeStaff(Team $team)
    {
        return $this->collection($team->staffLimitFive, new StaffTransformer);
    }
}