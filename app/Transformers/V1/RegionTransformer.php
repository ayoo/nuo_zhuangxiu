<?php

namespace App\Transformers\V1;

use App\Models\Region;

class RegionTransformer extends BaseTransformer
{
    public function transform(Region $region)
    {
        return $this->returnData([
            'id' => $region->id,
            'title' => $region->two_level_title,
            'estates' => $region->estates
        ]);
    }
}