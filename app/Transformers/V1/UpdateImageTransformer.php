<?php

namespace App\Transformers\V1;

use League\Fractal\TransformerAbstract;
use App\Models\UpdateImage;

class UpdateImageTransformer extends TransformerAbstract
{
    public function transform(UpdateImage $image)
    {
        return [
            'id' => $image->id,
            'url' => imageUrl($image->url)
        ];
    }
}