<?php 

namespace App\Transformers\Traits;

trait TransformerParams {

    protected $params = [];

    public function addParam() {
        $args = func_get_args();
        if(is_array($args[0]))
        {
            $this->params = $args[0];
        } else {
            $this->params[$args[0]] = $args[1];
        }
    }
}